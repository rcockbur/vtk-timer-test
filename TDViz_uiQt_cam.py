from PyQt4.QtGui import QApplication, QMainWindow, QFrame, QGridLayout, QWidget, QScrollBar, QLabel, QTabWidget, QPushButton, QHBoxLayout, QSpinBox, QFileDialog, QComboBox, QGroupBox, QVBoxLayout, QDial, QDialog, QSlider, QMenu, QLineEdit
from PyQt4.QtCore import Qt, QFile, QLatin1String, QSize, QObject, QEvent
from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
import vtk, sys, dicom, numpy as np, glob, xml.etree.ElementTree as ET, os, datetime, colorsys
from ui.widgets.transferfunction import TransferFunction, TransferFunctionWidget
from PySide.QtGui import QDialog as pysideQWidget
from PySide.QtGui import QGridLayout as pysideQGridLayout
import vrpn
import math
import time

isprojector = False

initfdir = r'T:\Data\Demo\Echo'
initddir = r'U:\Data\ProjectBrendan\Cases4Kumar'
tfuncdir = 'Presets\\TransferFunctions\\'
settings_dir = 'settings'
screenshot_dir = 'screenshots'

calibArray = np.array([1.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.541127, -0.840941, 0.000000, 0.000000, 0.840941, 0.541127, 0.000000, 0.000000, 0.000000, 0.000000, 1.000000])
calibMat = np.reshape(calibArray, (4,4))


class TDViz(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        
        self.frame = QFrame()
        self.vtkWidget = QVTKRenderWindowInteractor(self.frame)
        
        self.cropControlItems = CropControlItems(self)
        self.commonControlItems = CommonControlItems(self)
        self.positionControlItems = PositionControlItems(self)
        self.transferFunctionControlItems = TransferFunctionControlItems(self)
        self.planeWidgetControlItems = PlaneWidgetControlItems()
        self.playControlItems = PlayControlItems(self)
        self.smoothingControlItems = SmoothingControlItems(self)
        self.lightingControlItems = LightingControlItems()
        self.viewControlItems = ViewControlItems(self)
        self.labelControlItems = LabelControlItems()
        self.reportControlItems = ReportControlItems()
        
        tabWidget = QTabWidget()
        tabWidget.addTab(self.commonControlItems, "General Controls")
        tabWidget.addTab(self.viewControlItems, "View Controls")
        tabWidget.addTab(self.cropControlItems, "Cropping XYZ")
        tabWidget.addTab(self.planeWidgetControlItems, "Cropping Planes")
        tabWidget.addTab(self.transferFunctionControlItems, "Opacity && Color")
        tabWidget.addTab(self.positionControlItems, "Rotation && Position")
        tabWidget.addTab(self.playControlItems, "Play")
        tabWidget.addTab(self.smoothingControlItems, "Smoothing")
        tabWidget.addTab(self.lightingControlItems, "Lighting")
        tabWidget.addTab(self.labelControlItems, "Labelling")
        tabWidget.addTab(self.reportControlItems, "reporting")

        
        buttonGroup = QGroupBox()
        self.button_quit = QPushButton("Close")
        self.button_savesettings = QPushButton("Save Settings")
        self.label_loadsettings = QLabel("Load Settings: ")
        self.label_loadsettings.setAlignment(Qt.AlignTrailing)
        self.combobox_loadsettings = QComboBox()
        
        buttonLayout = QGridLayout()
        for index, button in enumerate((self.label_loadsettings, self.combobox_loadsettings, self.button_savesettings, self.button_quit)):
            buttonLayout.addWidget(button, index/2, index % 2 )
        buttonLayout.setSpacing(0)
        buttonLayout.setColumnStretch(0,4)
        buttonLayout.setColumnStretch(1,6)        
        
        buttonGroup.setLayout(buttonLayout)
        
        
        layout = QGridLayout()
        layout.addWidget(self.vtkWidget, 0, 0, 1, 2)
        layout.setRowStretch(0, 10)
        layout.addWidget(tabWidget, 1, 0)
        layout.addWidget(buttonGroup, 1, 1)
        layout.setSpacing(0)
        layout.setMargin(0)
        layout.setColumnStretch(0,10)
        layout.setColumnStretch(1,3)
        
        self.frame.setLayout(layout)
        self.setCentralWidget(self.frame)
        
        self.button_quit.clicked.connect(self.close)
        self.button_loadEcho.clicked.connect(self.loadEcho)
        self.button_box.clicked.connect(self.setBoxWidget)
        self.transferFunctionControlItems.combobox_transfunction.activated.connect(self.updateTFunc)
        self.button_resetcrop.clicked.connect(self.resetCrop)
        self.button_stereo.clicked.connect(self.setStereo)
        self.button_measurement.clicked.connect(self.setMeasurement)
        self.button_anglemeasurement.clicked.connect(self.setAngleMeasurement)
        self.button_loadDir.clicked.connect(self.loadDir)
        self.button_savesettings.clicked.connect(self.saveSettings)
        self.button_iterate.clicked.connect(self.playCardiacCycle)
        self.button_rotate.clicked.connect(self.rotateCamera)
        self.transferFunctionControlItems.button_edittransfunction.clicked.connect(self.editTransferFunction)
        self.transferFunctionControlItems.button_editopacity.clicked.connect(self.editOpacity)
        self.transferFunctionControlItems.button_editcolor.clicked.connect(self.editColor)
        self.transferFunctionControlItems.button_editgradient.clicked.connect(self.editGradientOpacity)
        self.transferFunctionControlItems.button_savetfunction.clicked.connect(self.saveTransferFunction)
        self.reportControlItems.report_button.clicked.connect(self.report)

        self.button_savescreen.clicked.connect(self.saveScreen)
        self.combobox_loadsettings.activated.connect(self.loadSettings)
        
            
        for scale in (self.scale_xmin, self.scale_xmax, self.scale_ymin, self.scale_ymax, self.scale_zmin, self.scale_zmax):
            scale.valueChanged.connect(self.cropVolume)
            
        for slider in (self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth):
            slider.valueChanged.connect(self.smoothVolume)
            
        self.button_nosmooth.clicked.connect(self.setNoSmooth)
        self.button_lowsmooth.clicked.connect(self.setLowSmooth)
        self.button_midsmooth.clicked.connect(self.setMidSmooth)
        self.button_highsmooth.clicked.connect(self.setHighSmooth)            

        self.scale_azimuth.valueChanged.connect(self.setAzimuth)
        self.scale_elevation.valueChanged.connect(self.setElevation)
        self.scale_roll.valueChanged.connect(self.setRoll)
        self.scale_stereodepth.valueChanged.connect(self.setStereoDepth)
        
        self.button_zoomin.clicked.connect(self.zoomIn)
        self.button_zoomout.clicked.connect(self.zoomOut)
        self.button_resetcamera.clicked.connect(self.resetCamera)
        
        self.slider_imageNumber.valueChanged.connect(self.slider_imageNumber_valuechanged)
        
        for i in range(6):
            self.planeWidgetControlItems.button_pwidgets[i].toggled.connect(self.setPlaneWidgets)
            self.planeWidgetControlItems.button_pwidgetreset[i].clicked.connect(self.resetPlaneWidget)
            
        self.planeWidgetControlItems.button_pwdigetresetall.clicked.connect(self.resetAllPlaneWidgets)
        self.button_cameratext.toggled.connect(self.displayCameraOrientation)
        
        self.lightingControlItems.button_shade.toggled.connect(self.setShade)
        self.lightingControlItems.button_interpolation.toggled.connect(self.setInterpolation)
        self.lightingControlItems.button_gradientopacity.toggled.connect(self.setDisableGradientOpacity)
#         self.button_sphereWidget.toggled.connect(self.setSphereWidget)
        self.lightingControlItems.slider_ambient.valueChanged.connect(self.adjustLights)
        self.lightingControlItems.slider_diffuse.valueChanged.connect(self.adjustLights)
        self.lightingControlItems.slider_specular.valueChanged.connect(self.adjustLights)
        
        self.lightingControlItems.slider_keylightintensity.valueChanged.connect(self.setKeyLightIntensity)
        
        self.labelControlItems.button_label.toggled.connect(self.displayLabel)
        self.labelControlItems.text_label.textEdited.connect(self.changeLabelText)
        self.labelControlItems.scale_labelsize.valueChanged.connect(self.changeLabelSize)
        self.labelControlItems.combobox_labels.currentIndexChanged[int].connect(self.changeLabelIndex)
                
        for button in self.button_view:
            button.clicked.connect(self.changeView)

        
    def getRenderWindow(self):
        return self.vtkWidget.GetRenderWindow()
    def report(self):
#         dwPoint1 = np.empty((3))
#         dwPoint2 = np.empty((3))
#         labelPoint1 = np.empty((3))
#         labelPoint2 = np.empty((3))
#         self.distanceRep.GetPoint1WorldPosition(dwPoint1)
#         self.distanceRep.GetPoint2WorldPosition(dwPoint2)
#         self.labelLineRep[0].GetPoint1WorldPosition(labelPoint1)
#         self.labelLineRep[1].GetPoint1WorldPosition(labelPoint2)  
#         print 'dw1='+str(dwPoint1)+'  dw2='+str(dwPoint2)
#         print 'label1='+str(labelPoint1)+'  label2='+str(labelPoint2)
        print 'Camera:'
        print 'pos=%s' %str(self.cam.GetPosition())
        print 'foc=%s' %str(self.cam.GetFocalPoint())
        print self._ren.GetActors().GetNumberOfItems()
        print self._ren.GetVolumes().GetNumberOfItems()

    
class vtkTimerHeadTrack():
    tracker=vrpn.receiver.Tracker("Tracker0@localhost")
    button=vrpn.receiver.Button("Tracker0@localhost")
    
    def __init__(self, cam, lineActor, volume, master):
        self.tracker.register_change_handler("tracker", self.callback, "position")
        self.button.register_change_handler("button", self.callback_button)
        
        self.cam = cam
        self.lineActor = lineActor
        self.volume = volume
        self.master = master
        
        transform = vtk.vtkTransform()
        self.lineActor.SetUserTransform(transform)
        transform = vtk.vtkTransform()
        self.volume.SetUserTransform(transform)
        
        self.deviceMat_np = np.zeros((4,4))
        self.deviceMatCal_np = np.zeros((4,4))
        self.deviceMatCal_vtk = vtk.vtkMatrix4x4()
        
        self.button0state = False
        self.button1state = False        
        self.button2state = False
        self.stylusMode0 = 0
        self.stylusMode1 = 0
        self.stylusMode2 = 0
        dwPoint1Distance = None
        dwPoint2Distance = None
        
        self.scale = 2.0       
        
        
        self.volumeTransform = vtk.vtkTransform()
        self.initialvolumeTransform = None
                
        
    def execute(self, obj, event):
        iren = obj
        if self.master.firstExecute == True:
            self.master.firstExecute = False
            self.master.afterInit()
        self.button.mainloop()
        self.tracker.mainloop()           
        iren.GetRenderWindow().Render()
           
        
    def callback_button(self, userdata, data):
        if data['button'] == 0:
            if data['state'] == 1:
                self.lineActor.GetProperty().SetColor(1.0,0.0,0.0)
                self.button0state = True
            elif data['state'] == 0:
                self.lineActor.GetProperty().SetColor(1.0,1.0,1.0)
                self.button0state = False                  
        if data['button'] == 1:
            if data['state'] == 1:
                self.lineActor.GetProperty().SetColor(0.0,0.0,1.0)
                self.button1state = True
            elif data['state'] == 0:
                self.lineActor.GetProperty().SetColor(1.0,1.0,1.0)
                self.button1state = False                   
        if data['button'] == 2:
            if data['state'] == 1:
                self.lineActor.GetProperty().SetColor(0.0,1.0,0.0)
                self.button2state = True
            elif data['state'] == 0:
                self.lineActor.GetProperty().SetColor(1.0,1.0,1.0)
                self.button2state = False     
    
    def callback(self, userdata, data):
        dx, dy, dz = data['position']
        qx, qy, qz, qw = data['quaternion']        
        sensorid = data['sensor']
        
        qwxyz = np.array([qw,qx,qy,qz])        
        vtk.vtkMath.QuaternionToMatrix3x3(qwxyz, self.deviceMat_np[0:3,0:3])
               
        self.deviceMat_np[0,3] = 1000.0*dx
        self.deviceMat_np[1,3] = 1000.0*dy
        self.deviceMat_np[2,3] = 1000.0*dz
        self.deviceMat_np[3,3] = 1.0
        
        self.deviceMatCal_np = calibMat.dot(self.deviceMat_np)
        
        if sensorid == 0:
#             self.text.SetInput("pos = (%-#6.3g, %-#6.3g, %-#6.3g)\n quaternion = (%-#6.3g, %-#6.3g, %-#6.3g, %-#6.3g)" % (dx, dy, dz, qw, qx, qy, qz))
            if not math.isnan(dx) and not math.isnan(qx):
                self.cam.SetEyeTransformMatrix(self.deviceMatCal_np.reshape(16,1))
                
        #Stylus
        elif sensorid == 1:       
            self.deviceMatCal_vtk.DeepCopy(self.deviceMatCal_np.reshape(16,1))
            transform = vtk.vtkTransform()
            transform.PostMultiply()
            transform.Concatenate(self.deviceMatCal_vtk)      # CalM * DevM * I
            modeltransform = vtk.vtkTransform()
            
            modeltransform.Concatenate(self.cam.GetModelViewTransformMatrix())                
            transform.Concatenate(modeltransform.GetInverse())                      #deviceMatCal_np -> calibMat_np -> inverse camMat
            self.lineActor.SetUserTransform(transform)
            self.lineActor.Modified()
            
            # get closest dw point, or none if not yet set
            # get closest label, or none if none placed
            
            stylusOrigin = np.array((0,0,-50))
            stylusPosition = np.empty((3))
            self.lineActor.GetUserTransform().TransformPoint(stylusOrigin, stylusPosition)
            
            
            if self.master.labelLine[0].GetEnabled():
                label0Position = np.empty((3))      
                self.master.labelLine[0].GetLineRepresentation().GetPoint1WorldPosition(label0Position)
                label0Distance = vtk.vtkMath.Distance2BetweenPoints(label0Position, stylusPosition)
                
            if self.master.labelLine[1].GetEnabled():
                label1Position = np.empty((3))      
                self.master.labelLine[1].GetLineRepresentation().GetPoint1WorldPosition(label1Position)
                label1Distance = vtk.vtkMath.Distance2BetweenPoints(label1Position, stylusPosition)
                
            closestLabelLine = None
            closestLabelActor = None
            if self.master.labelLine[0].GetEnabled() and not self.master.labelLine[1].GetEnabled():
                closestLabelLine = self.master.labelLine[0]
                closestLabelActor = self.master.labelActor[0]
            elif self.master.labelLine[1].GetEnabled() and not self.master.labelLine[0].GetEnabled():
                closestLabelLine = self.master.labelLine[1]
                closestLabelActor = self.master.labelActor[1]
            elif self.master.labelLine[0].GetEnabled() and self.master.labelLine[1].GetEnabled():
                if label0Distance < label1Distance:
                    closestLabelLine = self.master.labelLine[0]
                    closestLabelActor = self.master.labelActor[0]
                else:
                    closestLabelLine = self.master.labelLine[1]
                    closestLabelActor = self.master.labelActor[1]
            
            dwPoint1Distance = None
            dwPoint2Distance = None
            if self.master.dwPlaced:
                dwPoint1Position = np.empty((3))
                dwPoint2Position = np.empty((3))
                self.master.distanceWidget.GetDistanceRepresentation().GetPoint1WorldPosition(dwPoint1Position)
                self.master.distanceWidget.GetDistanceRepresentation().GetPoint2WorldPosition(dwPoint2Position)
                dwPoint1Distance = vtk.vtkMath.Distance2BetweenPoints(dwPoint1Position, stylusPosition)
                dwPoint2Distance = vtk.vtkMath.Distance2BetweenPoints(dwPoint2Position, stylusPosition)
                
            self.master.highlighter.GetProperty().SetOpacity(0)     
             
            if self.button0state: #Moves the object
                #Down press
                if self.stylusMode0 == 0:
                    self.stylusMode0 = 1
                    self.stylusTransformOld = vtk.vtkTransform()
                    self.stylusTransformOld.DeepCopy(self.lineActor.GetUserTransform())
                    self.volumeTransformOld = vtk.vtkTransform() 
                    self.volumeTransformOld.DeepCopy(self.volume.GetUserTransform())
                #Down held
                else:
                    self.volumeTransform = vtk.vtkTransform() 
                    self.volumeTransform.DeepCopy(self.volumeTransformOld)
                    self.volumeTransform.PostMultiply()
                    self.volumeTransform.Concatenate(self.stylusTransformOld.GetInverse())
                    self.volumeTransform.Concatenate(self.lineActor.GetUserTransform())
                    self.volume.SetUserTransform(self.volumeTransform)   
            else:
                #Up press
                if self.stylusMode0 == 1:
                    self.stylusMode0 = 0
                #Up held
                else:
                    pass
                
            if self.master.labelMode == True:
                if self.button1state: #Moves the labelLine
                    #Down press
                    if self.stylusMode1 == 0:
                        self.stylusMode1 = 1
                        
                        self.selectedLabelLine = None
                        self.selectedLabelActor = None
                        if self.master.labelLine[0].GetEnabled() or self.master.labelLine[1].GetEnabled():
                            self.selectedLabelLine = closestLabelLine
                            self.selectedLabelActor = closestLabelActor
                            self.labelPoint1 = np.empty((3))
                            self.labelPoint2 = np.empty((3))
                            self.labelPoint3 = np.empty((3))
                            self.selectedLabelLine.GetLineRepresentation().GetPoint1WorldPosition(self.labelPoint1)
                            self.selectedLabelLine.GetLineRepresentation().GetPoint2WorldPosition(self.labelPoint2)
                            self.labelPoint3[0],self.labelPoint3[1],self.labelPoint3[2] = self.selectedLabelActor.GetPosition()[0],self.selectedLabelActor.GetPosition()[1],self.selectedLabelActor.GetPosition()[2]                                
                            self.oldStylusMat = vtk.vtkTransform()
                            self.oldStylusMat.Concatenate(self.lineActor.GetMatrix())
                            self.master.highlightPosition(self.labelPoint3)
                                                       
                    #Down hold
                    else:
                        if self.master.labelLine[0].GetEnabled() or self.master.labelLine[1].GetEnabled():
                            transform = vtk.vtkTransform()
                            transform.Concatenate(self.lineActor.GetMatrix())
                            transform.Concatenate(self.oldStylusMat.GetInverse())
                            newPoint1 = np.empty((3))
                            newPoint2 = np.empty((3))
                            newPoint3 = np.empty((3))
                            transform.TransformPoint(self.labelPoint1, newPoint1)
                            transform.TransformPoint(self.labelPoint2, newPoint2)
                            transform.TransformPoint(self.labelPoint3, newPoint3)
                            self.selectedLabelLine.GetLineRepresentation().SetPoint1WorldPosition(newPoint1)
                            self.selectedLabelLine.GetLineRepresentation().SetPoint2WorldPosition(newPoint2)
                            self.selectedLabelActor.SetPosition(newPoint3)
                            self.master.labelCallback()
                            self.master.highlightPosition(newPoint1)
                            
                else:
                    #Up press
                    if self.stylusMode1 == 1:
                        self.stylusMode1 = 0
                    #Up held
                    else:
                        pass
                    if closestLabelLine:
                        self.master.highlightPosition(closestLabelLine.GetLineRepresentation().GetPoint1WorldPosition())
                        
            else:
                if self.button1state: #Moves the distance widget
                    #Down press
                    if self.stylusMode1 == 0:
                        self.stylusMode1 = 1 
                        
                        #First click
                        if self.master.dwPlaced == False:
                            nlabels = self.master.labelControlItems.combobox_labels.count()
                            for i in range(nlabels):
                                self.master.labelLine[i].ProcessEventsOff()
                            self.master.distanceWidget.EnabledOn()
                            self.master._ren.AddActor(self.master.distanceText) 
                            self.master.distanceWidget.GetDistanceRepresentation().SetPoint1WorldPosition(stylusPosition)
                            self.master.distanceWidget.GetDistanceRepresentation().SetPoint2WorldPosition(stylusPosition)
                        #Not first click
                        else:
                            if dwPoint1Distance > dwPoint2Distance:
                                self.setDwPoint = self.master.distanceWidget.GetDistanceRepresentation().SetPoint2WorldPosition
                                self.oldDwPoint = dwPoint2Position
                            else:
                                self.setDwPoint = self.master.distanceWidget.GetDistanceRepresentation().SetPoint1WorldPosition
                                self.oldDwPoint = dwPoint1Position
                            self.oldStylusMat = vtk.vtkTransform()
                            self.oldStylusMat.Concatenate(self.lineActor.GetMatrix())
                        self.master.dwStartInteraction(None, None)
                            
                    #Down held   
                    else:
                        #First click
                        if self.master.dwPlaced == False:
                            self.master.distanceWidget.GetDistanceRepresentation().SetPoint1WorldPosition(stylusPosition)
                        #Not first click
                        else:
                            transform = vtk.vtkTransform()
                            transform.Concatenate(self.lineActor.GetMatrix())
                            transform.Concatenate(self.oldStylusMat.GetInverse())
                            newDwPoint = np.empty((3))
                            transform.TransformPoint(self.oldDwPoint, newDwPoint)
                            self.setDwPoint(newDwPoint)
                        self.master.dwUpdateMeasurement(None, None)
                else:
                    #Up press
                    if self.stylusMode1 == 1:
                        self.stylusMode1 = 0
                        
                        self.master.dwCallback()
                        self.master.dwEndInteraction(None, None)
                        #First click
                        if self.master.dwPlaced == False:
                            self.master.dwPlaced = True
                    #Up held   
                    else:
                        pass
                if self.master.dwPlaced and dwPoint1Distance:
                    if dwPoint1Distance > dwPoint2Distance:
                        self.master.highlightPosition(dwPoint2Position)
                    else:           
                        self.master.highlightPosition(dwPoint1Position)
                            
            if self.button2state: #Scales everything
                #Down press
                if self.stylusMode2 == 0:
                    self.stylusMode2 = 1
                  
                    self.zOld = dz
                    self.scaleOld = self.volume.GetScale()[0]
                   
                    dwPoint1Old = np.empty((3))
                    dwPoint2Old = np.empty((3))
                    self.master.distanceWidget.GetDistanceRepresentation().GetPoint1WorldPosition(dwPoint1Old)
                    self.master.distanceWidget.GetDistanceRepresentation().GetPoint2WorldPosition(dwPoint2Old)
                    self.dwPoint1Relative = [dwPoint1Old[i] - self.volume.GetPosition()[i] for i in range(3)]
                    self.dwPoint2Relative = [dwPoint2Old[i] - self.volume.GetPosition()[i] for i in range(3)]
#                     self.master.dwStartInteraction(None, None)
                    
                #Down held
                else:                   
                    zDiff = dz - self.zOld
                    growthFactor = 1.0 + zDiff * 4.0
                    scaleNew = self.scaleOld * growthFactor
                    self.master.distanceScale = scaleNew
                    self.volume.RemoveAllObservers()
                    self.volume.SetScale(scaleNew)
                    self.volume.AddObserver(vtk.vtkCommand.AnyEvent, self.master.volumeCallback)
                    
                    dwPoint1Absolute = np.empty((3))
                    dwPoint2Absolute = np.empty((3))
                    for i in range(3):
                        dwPoint1Absolute[i] = self.dwPoint1Relative[i] * growthFactor + self.volume.GetPosition()[i]
                        dwPoint2Absolute[i] = self.dwPoint2Relative[i] * growthFactor + self.volume.GetPosition()[i]
                    self.master.distanceWidget.GetDistanceRepresentation().SetPoint1WorldPosition(dwPoint1Absolute)
                    self.master.distanceWidget.GetDistanceRepresentation().SetPoint2WorldPosition(dwPoint2Absolute)
                    self.master.updateDistanceText()
#                     self.master.dwUpdateMeasurement(None, None)
                    self.master.volumeCallback(None,None)
                    
            else:
                #Up press
                if self.stylusMode2 == 1:
                    self.stylusMode2 = 0
#                     self.master.dwEndInteraction(None,None)
                #Up held
                else:
                    pass
                
#             if self.master.labelMode:
#                 self.atLeastOneLabel = False
#                 if self.master.labelLine[0].GetEnabled() and not self.master.labelLine[1].GetEnabled():
#                     self.atLeastOneLabel = True
#                     self.labelLineX = self.master.labelLine[0]
#                 if self.master.labelLine[1].GetEnabled() and not self.master.labelLine[0].GetEnabled():
#                     self.atLeastOneLabel = True
#                     self.labelLineX = self.master.labelLine[1]
#                 if self.master.labelLine[0].GetEnabled() and self.master.labelLine[1].GetEnabled():
#                     self.atLeastOneLabel = True
#                     
#                     label0Position = np.empty((3))
#                     label1Position = np.empty((3))        
#                     self.master.labelLine[0].GetLineRepresentation().GetPoint1WorldPosition(label0Position)
#                     self.master.labelLine[1].GetLineRepresentation().GetPoint1WorldPosition(label1Position)
#                     
#                     stylusOrigin = np.array((0,0,-50))
#                     stylusPosition = np.empty((3))
#                     self.lineActor.GetUserTransform().TransformPoint(stylusOrigin, stylusPosition)
#                     
#                     label0Distance = vtk.vtkMath.Distance2BetweenPoints(label0Position, stylusPosition)
#                     dwPoint1Distance = vtk.vtkMath.Distance2BetweenPoints(label1Position, stylusPosition)
#                     if label0Distance > dwPoint1Distance:
#                         self.labelLineX = self.master.labelLine[1]
#                     else:
#                         self.labelLineX = self.master.labelLine[0]
#                 if self.atLeastOneLabel:       
#                     self.labelPoint = np.empty((3))
#                     self.labelLineX.GetLineRepresentation().GetPoint1WorldPosition(self.labelPoint)
#                     self.master.highlighter.SetPosition(self.labelPoint)
#                     self.master.highlighter.GetProperty().SetOpacity(1.0)     
#                             
#                             
#             if not self.master.labelMode and not self.master.dwPlaced:
#                 stylusOrigin = np.array((0,0,-50))
#                 stylusPosition = np.empty((3))
#                 self.lineActor.GetUserTransform().TransformPoint(stylusOrigin, stylusPosition)
#                     
#                 dwPoint1Position = np.empty((3))
#                 dwPoint2Position = np.empty((3))
#                 self.master.distanceWidget.GetDistanceRepresentation().GetPoint1WorldPosition(dwPoint1Position)
#                 self.master.distanceWidget.GetDistanceRepresentation().GetPoint2WorldPosition(dwPoint2Position)
#                 dwPoint1Distance = vtk.vtkMath.Distance2BetweenPoints(dwPoint1Position, stylusPosition)
#                 distanceTo2 = vtk.vtkMath.Distance2BetweenPoints(dwPoint2Position, stylusPosition)
#                 if dwPoint1Distance > distanceTo2:
#                     self.master.highlighter.SetPosition(dwPoint2Position)
#                 else:
#                     self.master.highlighter.SetPosition(dwPoint1Position)
#                 self.master.highlighter.GetProperty().SetOpacity(1.0)
                        

class vtkTimerSpaceNav():
    analog=vrpn.receiver.Analog("device0@localhost")
    
    def __init__(self, ren):
        self.timer_count = 0
        self.ren = ren
        self.cam = self.ren.GetActiveCamera()
        self.transform = vtk.vtkTransform()
        self.analog.register_change_handler("analog", self.callback)
        
    def execute(self, obj, event):
        iren = obj
        self.analog.mainloop()
            
        iren.GetRenderWindow().Render()    
    
    def callback(self,userdata, data):
        ch = data['channel']
        self.cam.Azimuth(ch[5])
        self.cam.Elevation(ch[3])
        self.cam.Roll(ch[4])
        self.cam.OrthogonalizeViewUp()
        self.cam.ApplyTransform(self.transform)
        self.ren.ResetCameraClippingRange()


class CropControlItems(QWidget):
    def __init__(self, master):
        super(CropControlItems, self).__init__()
        
        nscales = 6
        
        master.scale_xmin, master.scale_xmax, master.scale_ymin, master.scale_ymax, master.scale_zmin, master.scale_zmax = [QScrollBar(Qt.Horizontal) for i in range(nscales)]
        label_xmin, label_xmax, label_ymin, label_ymax, label_zmin, label_zmax = [QLabel() for i in range(nscales)]
        
        master.button_resetcrop = QPushButton("Crop Reset")
        master.button_box = QPushButton("BoxWidget On/Off",)
        layout = QGridLayout()            
        for index, label, labeltext, labelcolor in zip(range(nscales), (label_xmin, label_xmax, label_ymin, label_ymax, label_zmin, label_zmax), ("X min", "X max", "Y min", "Y max", "Z min", "Z max"),\
                                                       ("red", "red", "green", "green", "blue", "blue")):
            label.setText(labeltext)
            label.setStyleSheet("QLabel {color:" + labelcolor +" ; }");
            layout.addWidget(label, 0, index)
            
        layout.addWidget(master.button_box, 0, index + 1)
                    
        for index, scale in enumerate((master.scale_xmin, master.scale_xmax, master.scale_ymin, master.scale_ymax, master.scale_zmin, master.scale_zmax, master.button_resetcrop)):            
            scale.setEnabled(False)
            layout.addWidget(scale, 1, index)

        master.button_box.setEnabled(False)
            
        layout.setMargin(5)
        layout.setHorizontalSpacing(20)       
        layout.setVerticalSpacing(0)     
                    
        self.setLayout(layout)
        
class ReportControlItems(QWidget):
    def __init__(self):
        super(ReportControlItems, self).__init__()
        
        self.report_button= QPushButton('report')
        layout = QGridLayout()  
        self.report_button.setCheckable(True)
        layout.addWidget(self.report_button,0,0)
        
            
        layout.setMargin(5)
        layout.setVerticalSpacing(0)
                    
        self.setLayout(layout)          
        
class PlaneWidgetControlItems(QWidget):
    def __init__(self):
        super(PlaneWidgetControlItems, self).__init__()
        
        nwidgets = 6
        
        self.button_pwidgets = [QPushButton() for i in range(nwidgets)]
        self.button_pwidgetreset = [QPushButton() for i in range(nwidgets)]
        self.button_pwdigetresetall = QPushButton("Reset All")
        self.button_pwdigetresetall.setEnabled(False)
        layout = QGridLayout()  
        for i in range(nwidgets):
            self.button_pwidgets[i].setText("Plane Widget - " + str(i+1))
            self.button_pwidgets[i].setCheckable(True)
            self.button_pwidgets[i].setEnabled(False)
            layout.addWidget(self.button_pwidgets[i],0,i)
            
            self.button_pwidgetreset[i].setText("Reset - " + str(i+1))
            self.button_pwidgetreset[i].setObjectName('resetplane%d' % i)
            self.button_pwidgetreset[i].setEnabled(False)
            layout.addWidget(self.button_pwidgetreset[i],1,i)
        
        layout.addWidget(self.button_pwdigetresetall,1,i+1)
            
        layout.setMargin(5)
        layout.setVerticalSpacing(0)
                    
        self.setLayout(layout) 

class PlayControlItems(QWidget):
    def __init__(self, master):
        super(PlayControlItems, self).__init__()
        master.slider_imageNumber = QSlider(Qt.Horizontal)
        master.slider_imageNumber.setPageStep(1)
        master.label_imageNumber = QLabel('')
        master.label_imageNumber.setFixedWidth(80)
        master.button_iterate = QPushButton("Play/Stop")
        layout = QGridLayout() 
        layout.addWidget(master.label_imageNumber,0,0)
        layout.addWidget(master.slider_imageNumber,1,0)
        layout.addWidget(master.button_iterate,1,1)
        layout.setMargin(5)
                            
        for col, val in enumerate((4,1)):
            layout.setColumnStretch(col,val)  
            
        master.slider_imageNumber.setEnabled(False)
        master.button_iterate.setEnabled(False)      
    
        self.setLayout(layout)      

class SmoothingControlItems(QWidget):
    def __init__(self, master):
        super(SmoothingControlItems, self).__init__()    
        
        master.button_nosmooth = QPushButton('No Smoothing')
        master.button_lowsmooth = QPushButton('Low Smoothing')
        master.button_midsmooth = QPushButton('Medium Smoothing')
        master.button_highsmooth = QPushButton('High Smoothing')
        
        master.slider_xsmooth = QSlider(Qt.Horizontal)    
        master.slider_ysmooth = QSlider(Qt.Horizontal)
        master.slider_zsmooth = QSlider(Qt.Horizontal)
        
        master.label_xsmooth = QLabel('')
        master.label_ysmooth = QLabel('')
        master.label_zsmooth = QLabel('')

        layout = QGridLayout() 
        
        for index, button in enumerate((master.button_nosmooth, master.button_lowsmooth, master.button_midsmooth, master.button_highsmooth)):
            layout.addWidget(button, 1, index)
        
        layout.addWidget(master.label_xsmooth,0,index+1)
        layout.addWidget(master.label_ysmooth,0,index+2)
        layout.addWidget(master.label_zsmooth,0,index+3)
        
        layout.addWidget(master.slider_xsmooth,1,index+1)
        layout.addWidget(master.slider_ysmooth,1,index+2)
        layout.addWidget(master.slider_zsmooth,1,index+3)
        
        
        layout.setMargin(5)
        
        self.setLayout(layout) 
        
class LightingControlItems(QWidget):
    def __init__(self):
        super(LightingControlItems, self).__init__()
        
        self.button_shade = QPushButton('Shade On/Off')
        self.button_interpolation = QPushButton('Interpolation: Linear/NN ')
        self.button_gradientopacity = QPushButton('Gradient Opacity On/Off')

        for comp in (self.button_shade, self.button_interpolation, self.button_gradientopacity):
            comp.setCheckable(True)
        
        self.slider_ambient = QSlider(Qt.Horizontal)    
        self.slider_diffuse = QSlider(Qt.Horizontal)
        self.slider_specular = QSlider(Qt.Horizontal)                
        self.slider_keylightintensity = QSlider(Qt.Horizontal)
        self.slider_ambient.setValue(100.0)
        self.slider_diffuse.setValue(100.0)        
        self.slider_keylightintensity.setValue(20)
              
        for comp in (self.button_shade, self.button_interpolation, self.button_gradientopacity, self.slider_ambient, self.slider_diffuse, self.slider_specular, self.slider_keylightintensity):
            comp.setEnabled(False)
        
        self.label_ambient = QLabel('Ambient: 1.0')
        self.label_diffuse = QLabel('Diffuse: 1.0')
        self.label_specular = QLabel('Specular: 0.0')

        self.label_keylightintensity = QLabel('Key Light Intensity: 4.0')

        layout = QGridLayout() 
        
        for ind, comp in enumerate((self.label_ambient, self.label_diffuse, self.label_specular, self.label_keylightintensity)):
            layout.addWidget(comp,0,ind)
        
        for ind, comp in enumerate((self.slider_ambient, self.slider_diffuse, self.slider_specular, self.slider_keylightintensity)):
            layout.addWidget(comp,1,ind)
        
        layout.addWidget(self.button_interpolation,0,ind+1)
        layout.addWidget(self.button_gradientopacity,1,ind+1)
        layout.addWidget(self.button_shade,1,ind+2)
        
#         for col in range(ind+2):
#             layout.setColumnStretch(col, 5)    
        
        layout.setMargin(5)
        layout.setVerticalSpacing(0)
                
        self.setLayout(layout) 
        
class ViewControlItems(QWidget):
    def __init__(self, master):
        super(ViewControlItems, self).__init__()
        
        nbuttons = 10
        master.button_view = [QPushButton() for i in range(nbuttons)]
        
        self.label_view = QLabel("View")
        self.label_roll = QLabel("Roll")
        
        buttontext = ["XY", "YZ", "ZX", "-XY", "-YZ", "-ZX", "0", "90", "180", "270"]
 
        layout = QGridLayout()
        
        for index in range(nbuttons):
            master.button_view[index].setText(buttontext[index])
            master.button_view[index].setObjectName('button_view%d' % index)
            layout.addWidget(master.button_view[index], 1, index)

        layout.addWidget(self.label_view,0,0)
        layout.addWidget(self.label_roll,0,6)
        
        layout.setMargin(5)
        layout.setHorizontalSpacing(5)       
        layout.setVerticalSpacing(0)                  
        self.setLayout(layout)
            
class LabelControlItems(QWidget):
    def __init__(self):
        super(LabelControlItems, self).__init__()
        
        nlabels = 2
        
        self.combobox_labels = QComboBox()
        self.label_label = QLabel("Label: ")
        self.label_text = QLabel("Text: ")
        self.text_label = QLineEdit("Label1")
        self.button_label = QPushButton("On/Off")
        
        self.scale_labelsize = QSlider(Qt.Horizontal)
        self.label_labelsize = QLabel("Label Size")
        self.scale_labelsize.setMinimum(1)
        self.scale_labelsize.setValue(20)
        
        self.button_label.setCheckable(True)
        
        for i in range(nlabels):
            self.combobox_labels.addItem("Label"+str(i+1))
            
        layout = QGridLayout()
        
        layout.addWidget(self.label_label,0,0)
        layout.addWidget(self.combobox_labels,1,0)
        layout.addWidget(self.label_text,0,1)
        layout.addWidget(self.text_label,1,1)
        layout.addWidget(self.button_label,1,2)

        layout.addWidget(self.label_labelsize,0,3)
        layout.addWidget(self.scale_labelsize,1,3)
        self.setLayout(layout)



                   
class CommonControlItems(QWidget):
    def __init__(self, master):
        self.master = master
        super(CommonControlItems, self).__init__()
        
        nbuttons = 8
        
        master.button_loadEcho, master.button_loadDir, master.button_rotate, \
            master.button_stereo, master.button_measurement, master.button_anglemeasurement,  master.button_cameratext, master.button_savescreen = [QPushButton() for i in range(nbuttons)]                    
        buttontext = ["Load MR/CT", "Load Echo",  "Rotate/Stop", "Stereo On/Off", "Distance Meas. On/Off", "Angle Meas. On/Off", "Display Orientation", "Save Screen"]
               
        master.button_cameratext.setCheckable(True)
        
        layout = QHBoxLayout() 
        layout.setSpacing(0)
        for index, button in enumerate((master.button_loadDir,  master.button_loadEcho, master.button_rotate, \
            master.button_stereo, master.button_measurement, master.button_anglemeasurement, master.button_cameratext, master.button_savescreen )):
            button.setText(buttontext[index])
            
        for comp in ((master.button_loadDir, master.button_loadEcho, master.button_rotate,  \
            master.button_stereo, master.button_measurement, master.button_anglemeasurement, master.button_cameratext, master.button_savescreen )):
            layout.addWidget(comp)
            
        self.setLayout(layout)
        
class PositionControlItems(QWidget):
    def __init__(self, master):
        self.master = master        
        super(PositionControlItems, self).__init__()

        nscales = 4
#         master.scale_azimuth, master.scale_elevation, master.scale_roll, master.scale_stereodepth = [QScrollBar(Qt.Horizontal) for i in range(nscales)]
        master.scale_azimuth, master.scale_elevation, master.scale_roll = [QDial() for i in range(nscales-1)]
        master.scale_stereodepth = QScrollBar(Qt.Horizontal)
        label_azimuth, label_elevation, label_roll, label_stereodepth = [QLabel() for i in range(nscales)]
        master.button_zoomin, master.button_zoomout, master.button_resetcamera = [QPushButton() for i in range(3)]
        label_stereodepth = QLabel("Stereo depth")
        
        for button, buttontext in zip((master.button_zoomin, master.button_zoomout, master.button_resetcamera),("Zoom In", "Zoom Out", "Reset")):
            button.setText(buttontext)
        
        layout = QGridLayout()
        for index, label, labeltext in zip(range(nscales), (label_azimuth, label_elevation, label_roll), ("Azimuth", "Elevation", "Roll")):
            label.setText(labeltext)
            label.setAlignment(Qt.AlignRight)
#             layout.addWidget(labelLine, 0, index)
        
        layout.addWidget(master.button_zoomin, 0, 7)
        layout.addWidget(master.button_zoomout, 0, 8)
        
        for index, scale in enumerate((master.scale_azimuth, master.scale_elevation, master.scale_roll)):
#             scale.setEnabled(False)
            scale.setMinimum(-179)
            scale.setMaximum(180)
            scale.setValue(0)
            scale.setMaximumSize(QSize(60,60))
#             layout.addWidget(scale,1,index)
            
        for index, comp in enumerate((label_azimuth, master.scale_azimuth,label_elevation,  master.scale_elevation, label_roll, master.scale_roll)):
            layout.addWidget(comp,0,index, 2, 1)
        
        layout.addWidget(master.button_resetcamera, 1, 8)
            
        master.scale_stereodepth.setValue(20)            
        master.scale_stereodepth.setMinimum(10)
        master.scale_stereodepth.setMaximum(100)
        layout.addWidget(label_stereodepth,0,6)            
        layout.addWidget(master.scale_stereodepth,1,6)
        
        layout.setMargin(0)
        layout.setHorizontalSpacing(20)       
        layout.setVerticalSpacing(0)     
        
        for col, val in enumerate((1,2,1,2,1,2,4,4,4)):
            layout.setColumnStretch(col,val)
                    
        self.setLayout(layout)
        
class TransferFunctionControlItems(QWidget):
    def __init__(self, master):
        super(TransferFunctionControlItems, self).__init__()
        self.button_edittransfunction, self.button_editopacity, self.button_editcolor, self.button_editgradient, self.button_savetfunction = [QPushButton() for i in range(5)]
        buttontext = ["Edit Transfer Function", "Edit Opacity", "Edit Color", "Edit Gradient Opacity", "Save Transfer Function"]        
     
        self.combobox_transfunction = QComboBox()
        label_transfunction = QLabel("Transfer Function")
        label_transfunction.setAlignment(Qt.AlignCenter)
     
        for index, button in enumerate((self.button_edittransfunction, self.button_editopacity, self.button_editcolor, self.button_editgradient, self.button_savetfunction)):
            button.setText(buttontext[index])
     
        layout = QHBoxLayout() 
        layout.setSpacing(0)
        for comp in ((label_transfunction, self.combobox_transfunction, self.button_edittransfunction, self.button_editopacity, self.button_editcolor, self.button_editgradient, self.button_savetfunction)):
            layout.addWidget(comp)
     
        self.setLayout(layout)

class vtkTimerCallBack():
    def __init__(self, K, isplay, isrotate, slider_imageNumber):
        self.timer_count = 0
        self.K = K
        self.isplay = isplay
        self.isrotate = isrotate
        self.slider_imageNumber = slider_imageNumber
        
    def execute(self, obj, event):
        iren = obj
        if self.isplay: 
            self.slider_imageNumber.setValue(self.timer_count % self.K)
            self.timer_count += 1
            
        if self.isrotate:
            self.renderer.GetActiveCamera().Azimuth(-1)  
            
        iren.GetRenderWindow().Render()
        
    def setplay(self, isplay):
        self.isplay = isplay
        
    def setrotate(self, isrotate):
        self.isrotate = isrotate         

class OpacityEditor(QDialog):
    def __init__(self, volumeProperty, reader, renWin):
        super(OpacityEditor, self).__init__()  
#        self.setModal(True)
        self.volumeProperty = volumeProperty
        self.reader = reader
        self.renWin = renWin
        self.setWindowTitle("Opacity Editor") 
        self.opacityfunction =  self.volumeProperty.GetScalarOpacity(0)
        self.npts = self.opacityfunction.GetSize()
        
        self.vScale = [[QSlider(Qt.Horizontal) for i in range(4)] for j in range(self.npts)]  
        self.label_value = [[QLabel(" ") for i in range(4)] for j in range(self.npts)]  
        
        label_scaleName = [QLabel() for j in range(4)]
        for j, text in enumerate(("Intensity", "Opacity","Midpoint","Sharpness")):
            label_scaleName[j].setText(text)       
        
        layout = QGridLayout()
        
        for j in range(4):
            layout.addWidget(label_scaleName[j],0,2*j)
        
        rmax = self.reader.GetOutput().GetScalarRange()[1] if self.reader.GetOutput().GetScalarRange()[1]> self.opacityfunction.GetRange()[1] else self.opacityfunction.GetRange()[1]
        rmin = self.reader.GetOutput().GetScalarRange()[0] if self.reader.GetOutput().GetScalarRange()[0] < self.opacityfunction.GetRange()[0] else self.opacityfunction.GetRange()[0]
        opacityNode = np.empty((4,))

        for i in range(self.npts):
            self.opacityfunction.GetNodeValue(i, opacityNode)
            for j in range(4):
                layout.addWidget(self.label_value[i][j],2*i,2*j+1)
                layout.addWidget(self.vScale[i][j],2*i+1,2*j,1,2)  
                if j==0:
                    self.vScale[i][j].setMinimum(rmin)
                    self.vScale[i][j].setMaximum(rmax)
                    self.vScale[i][j].setValue(opacityNode[j])
                else:        
                    self.vScale[i][j].setValue(100*opacityNode[j])

                self.vScale[i][j].valueChanged.connect(self.updateOpacity)
        
        self.updateOpacity()
        
        layout.setSpacing(0)
        layout.setHorizontalSpacing(10)
        self.setLayout(layout)
        self.resize(400,50*self.npts)
        
    def updateOpacity(self):
        self.opacityfunction.RemoveAllPoints()
        for i in range(self.npts):
            self.opacityfunction.AddPoint(self.vScale[i][0].value(),0.01*self.vScale[i][1].value(),0.01*self.vScale[i][2].value(),0.01*self.vScale[i][3].value())

            for j in range(4):
                if j == 0:
                    self.label_value[i][j].setText(str(self.vScale[i][j].value()))
                else:
                    self.label_value[i][j].setText(str(0.01*self.vScale[i][j].value()))
            
        self.volumeProperty.SetScalarOpacity(self.opacityfunction)
        self.renWin.Render()            

class GradientOpacityEditor(QDialog):
    def __init__(self, volumeProperty, reader, renWin):
        super(GradientOpacityEditor, self).__init__()  
#        self.setModal(True)
        self.volumeProperty = volumeProperty
        self.reader = reader
        self.renWin = renWin
        self.setWindowTitle("Gradient Opacity Editor") 
        self.opacityfunction =  self.volumeProperty.GetGradientOpacity(0)
        self.npts = self.opacityfunction.GetSize()
        
        self.vScale = [[QSlider(Qt.Horizontal) for i in range(4)] for j in range(self.npts)]  
        self.label_value = [[QLabel(" ") for i in range(4)] for j in range(self.npts)]  
        
        label_scaleName = [QLabel() for j in range(4)]
        for j, text in enumerate(("Intensity", " Opacity","Midpoint","Sharpness")):
            label_scaleName[j].setText(text)       
        
        layout = QGridLayout()
        
        for j in range(4):
            layout.addWidget(label_scaleName[j],0,2*j)
        
        rmax = self.reader.GetOutput().GetScalarRange()[1] if self.reader.GetOutput().GetScalarRange()[1]> self.opacityfunction.GetRange()[1] else self.opacityfunction.GetRange()[1]
        rmin = self.reader.GetOutput().GetScalarRange()[0] if self.reader.GetOutput().GetScalarRange()[0] < self.opacityfunction.GetRange()[0] else self.opacityfunction.GetRange()[0]
        opacityNode = np.empty((4,))

        for i in range(self.npts):
            self.opacityfunction.GetNodeValue(i, opacityNode)
            for j in range(4):
                layout.addWidget(self.label_value[i][j],2*i,2*j+1)
                layout.addWidget(self.vScale[i][j],2*i+1,2*j,1,2)  
                if j==0:
                    self.vScale[i][j].setMinimum(rmin)
                    self.vScale[i][j].setMaximum(rmax)
                    self.vScale[i][j].setValue(opacityNode[j])
                else:        
                    self.vScale[i][j].setValue(100*opacityNode[j])

                self.vScale[i][j].valueChanged.connect(self.updateOpacity)
        
        self.updateOpacity()
        
        layout.setSpacing(0)
        layout.setHorizontalSpacing(10)
        self.setLayout(layout)
        self.resize(400,50*self.npts)
        
    def updateOpacity(self):
        self.opacityfunction.RemoveAllPoints()
        for i in range(self.npts):
            self.opacityfunction.AddPoint(self.vScale[i][0].value(),0.01*self.vScale[i][1].value(),0.01*self.vScale[i][2].value(),0.01*self.vScale[i][3].value())

            for j in range(4):
                if j == 0:
                    self.label_value[i][j].setText(str(self.vScale[i][j].value()))
                else:
                    self.label_value[i][j].setText(str(0.01*self.vScale[i][j].value()))
            
        self.volumeProperty.SetGradientOpacity(self.opacityfunction)
        self.renWin.Render()   
        
class ColorEditor(QDialog):        
    def __init__(self, volumeProperty, reader, renWin):
        super(ColorEditor, self).__init__()  
#         self.setModal(True)
        self.volumeProperty = volumeProperty
        self.reader = reader
        self.renWin = renWin
        self.setWindowTitle("Color Editor") 
        
        self.colorfunction =  self.volumeProperty.GetRGBTransferFunction(0)
        self.npts = self.colorfunction.GetSize() 
        
        self.vScale = [[QSlider(Qt.Horizontal) for i in range(6)] for j in range(self.npts)]  
        self.label_value = [[QLabel(" ") for i in range(6)] for j in range(self.npts)]  
        
        label_scaleName = [QLabel() for j in range(6)]
        for j, text in enumerate(("Intensity", "Red","Green","Blue","Midpoint","Sharpness")):
            label_scaleName[j].setText(text)       
        
        layout = QGridLayout()
        
        for j in range(6):
            layout.addWidget(label_scaleName[j],0,2*j)
        
        rmax = self.reader.GetOutput().GetScalarRange()[1] if self.reader.GetOutput().GetScalarRange()[1]> self.colorfunction.GetRange()[1] else self.colorfunction.GetRange()[1]
        rmin = self.reader.GetOutput().GetScalarRange()[0] if self.reader.GetOutput().GetScalarRange()[0] < self.colorfunction.GetRange()[0] else self.colorfunction.GetRange()[0]
        opacityNode = np.empty((6,))

        for i in range(self.npts):
            self.colorfunction.GetNodeValue(i, opacityNode)
            for j in range(6):
                layout.addWidget(self.label_value[i][j],2*i,2*j+1)
                layout.addWidget(self.vScale[i][j],2*i+1,2*j,1,2)  
                if j==0:
                    self.vScale[i][j].setMinimum(rmin)
                    self.vScale[i][j].setMaximum(rmax)
                    self.vScale[i][j].setValue(opacityNode[j])
                else:        
                    self.vScale[i][j].setValue(100*opacityNode[j])

                self.vScale[i][j].valueChanged.connect(self.updateColor)
        
        self.updateColor()
        
        layout.setSpacing(0)
        layout.setHorizontalSpacing(10)
        self.setLayout(layout)
        self.resize(600,50*self.npts)
        
    def updateColor(self):
        self.colorfunction.RemoveAllPoints()
        for i in range(self.npts):
            self.colorfunction.AddRGBPoint(self.vScale[i][0].value(),0.01*self.vScale[i][1].value(),0.01*self.vScale[i][2].value(),0.01*self.vScale[i][3].value(),0.01*self.vScale[i][4].value(),0.01*self.vScale[i][5].value())

            for j in range(6):
                if j == 0:
                    self.label_value[i][j].setText(str(self.vScale[i][j].value()))
                else:
                    self.label_value[i][j].setText(str(0.01*self.vScale[i][j].value()))
            
        self.volumeProperty.SetColor(self.colorfunction)
        self.renWin.Render()         
               
class TransferFunctionEditor(pysideQWidget): 
    def __init__(self, volumeProperty, reader, renWin):        
        super(TransferFunctionEditor, self).__init__()    
#         self.setModal(True)    
        self.volumeProperty = volumeProperty
        self.reader = reader
        self._renderWindow = renWin
        self.setWindowTitle("Transfer Function Editor") 
        
        self.opacityfunction =  self.volumeProperty.GetScalarOpacity(0)
        self.colorfunction =  self.volumeProperty.GetRGBTransferFunction(0)

        self.npts = self.opacityfunction.GetSize()
        
        imageData = reader.GetOutput()
        self.histogramWidget = TransferFunctionWidget()
        transferFunction = TransferFunction()
        
        rmax = self.reader.GetOutput().GetScalarRange()[1] 
        rmin = self.reader.GetOutput().GetScalarRange()[0]         
        transferFunction.setRange([rmin, rmax])
        
        self.minimum, self.maximum = rmin, rmax
        
        opacityNode = np.empty((4,))
        transferFunction.addPoint(rmin, self.opacityfunction.GetValue(rmin), color=[self.colorfunction.GetRedValue(rmin), self.colorfunction.GetGreenValue(rmin), self.colorfunction.GetBlueValue(rmin)])
        for i in range(self.npts):
            self.opacityfunction.GetNodeValue(i, opacityNode)
            if (opacityNode[0] > rmin) and (opacityNode[0] < rmax):
                transferFunction.addPoint(opacityNode[0], opacityNode[1], color=[self.colorfunction.GetRedValue(opacityNode[0]), self.colorfunction.GetGreenValue(opacityNode[0]), self.colorfunction.GetBlueValue(opacityNode[0])])       
            
        transferFunction.addPoint(rmax, self.opacityfunction.GetValue(rmax), color=[self.colorfunction.GetRedValue(rmax), self.colorfunction.GetGreenValue(rmax), self.colorfunction.GetBlueValue(rmax)])
        
        transferFunction.updateTransferFunction() 
        
        self.histogramWidget.transferFunction = transferFunction 
        self.histogramWidget.setImageData(imageData)        
        self.histogramWidget.transferFunction.updateTransferFunction()        
        self.histogramWidget.updateNodes()  
        
        self.histogramWidget.valueChanged.connect(self.valueChanged)

        self.resize(400,300)
#                     if isprojector:
#                 self.transferFunctionEditor.setGeometry(1920,600,400,200)
#             else:
#                 self.transferFunctionEditor.setGeometry(300,300,400,200)
        
    def getTransferFunctionWidget(self):
        return self.histogramWidget        
    
    def updateTransferFunction(self):
        if self.histogramWidget:
            self.histogramWidget.transferFunction.updateTransferFunction()
            self.colorFunction = self.histogramWidget.transferFunction.colorFunction
            self.opacityFunction = self.histogramWidget.transferFunction.opacityFunction
        else:
            # Transfer functions and properties
            self.colorFunction = vtkColorTransferFunction()
            self.colorFunction.AddRGBPoint(self.minimum, 0, 0, 0)
            self.colorFunction.AddRGBPoint(self.maximum, 0, 0, 0)

            self.opacityFunction = vtkPiecewiseFunction()
            self.opacityFunction.AddPoint(self.minimum, 0)
            self.opacityFunction.AddPoint(self.maximum, 0)

        self.volumeProperty.SetColor(self.colorFunction)
        self.volumeProperty.SetScalarOpacity(self.opacityFunction)

#         self.volumeCT.SetProperty(self.volProp)  
        self._renderWindow.Render() 
        
    def valueChanged(self, value):
        self.updateTransferFunction()
        
class TDVizCustom(TDViz):      
    def __init__(self, parent=None):
        TDViz.__init__(self, parent)
        
        self.dirname = None
        
        self._renWin = self.getRenderWindow()
        self._renWin.StereoCapableWindowOn()
        self._renWin.SetStereoTypeToCrystalEyes()
#         self._renWin.SetStereoTypeToDresden()
        self._renWin.StereoRenderOn()  
#         self._renWin.HideCursor()
        
        self._ren = vtk.vtkRenderer()
        self.volume = vtk.vtkVolume()
        self.volumeProperty = vtk.vtkVolumeProperty()        
        
        self._renWin.AddRenderer(self._ren)
        
        self._iren = self._renWin.GetInteractor()  
        self._iren.SetInteractorStyle(vtk.vtkInteractorStyleTrackballActor())
#         self.removeAllMouseEvents(self._iren)
        
        self.sopuid = []
        self.reader = []
        
        self.initAxes()  
#         self.initBoxWidget()
        self.initMeasurementTool()
        self.initAngleMeasurementTool()
#         self.initPlaneWidget()
        self.initSphereWidget()
        self.initLabels()
        self.initHighlighter()
        
        self.last_distw_p1, self.last_distw_p2, self.distw_p1, self.distw_p2 = [np.empty((3)) for i in range(4)]
        self.label1_pt, self.label2_pt  = [np.empty((3)) for i in range(2)]
        
        tfuncs = glob.glob1(tfuncdir, "*.vvt")
        self.transferFunctionControlItems.combobox_transfunction.addItems(tfuncs)        

        self.varscaleazimuth = self.scale_azimuth.value()
        self.varscaleelevation = self.scale_elevation.value()

        self._ren.GetActiveCamera().AddObserver("AnyEvent", self.cameraAnyEvent)
        
        self.imageGaussianSmooth = vtk.vtkImageGaussianSmooth()
        
        self._renWin.Render()  

        self.cam = self._ren.GetActiveCamera()
        self.cam.UseOffAxisProjectionOn()
        self.cam.SetUseOffAxisProjection(1)
        self.cam.SetEyeSeparation(60)        
        
        self.initHeadTrackText()
        
        self._renWin.Render()
        
        self.labelMode = False  
        self.firstRender = True
    def removeAllMouseEvents(self, obj):        
        obj.RemoveObservers('LeftButtonPressEvent')
        obj.RemoveObservers('RightButtonPressEvent')
        obj.RemoveObservers('ButtonPressEvent')
        obj.RemoveObservers('LeftButtonReleaseEvent')
        obj.RemoveObservers('RightButtonReleaseEvent')
        obj.RemoveObservers('ButtonReleaseEvent')
        obj.RemoveObservers('MouseMoveEvent')
        obj.RemoveObservers('MouseWheelForwardEvent')
        obj.RemoveObservers('MouseWheelBackwardEvent')
        obj.RemoveObservers('MouseMoveEvent')        
        
    def initHeadTrackText(self):
        self.timertext = vtk.vtkTextActor()        
        self.timertext.SetDisplayPosition(1600, 20)      
        self.timertext.SetInput("time = ")
        self._ren.AddActor(self.timertext)  

        self.dirtext = vtk.vtkTextActor()        
        self.dirtext.SetDisplayPosition(20, 20)      
        self.dirtext.SetInput("None")
        self._ren.AddActor(self.dirtext)
        
    def initStylus(self):
        sphere = vtk.vtkSphereSource()
        sphere.SetThetaResolution(30)
        sphere.SetPhiResolution(30)
        sphere.SetRadius(2)

        sphereTransform = vtk.vtkTransform()
        sphereTransform.Translate(0, 0, -50)        
        
        sphereTransformFilter = vtk.vtkTransformPolyDataFilter()
        sphereTransformFilter.SetInputConnection(sphere.GetOutputPort())
        sphereTransformFilter.SetTransform(sphereTransform)
        
        line = vtk.vtkLineSource()
        line.SetResolution(30)
        line.SetPoint1(0.0, 0.0, 0.0)
        line.SetPoint2(0.0, 0.0, -50)
        
        appendFilter = vtk.vtkAppendPolyData()
        appendFilter.AddInputConnection(line.GetOutputPort())
        appendFilter.AddInputConnection(sphereTransformFilter.GetOutputPort())
        
        lineMapper = vtk.vtkPolyDataMapper()
        lineMapper.SetInputConnection(appendFilter.GetOutputPort())
        
        self.lineActor = vtk.vtkActor()
        self.lineActor.SetMapper(lineMapper)    
        
        self._ren.AddActor(self.lineActor)  
        
    def initHighlighter(self):
        sphere = vtk.vtkSphereSource()
        sphere.SetThetaResolution(30)
        sphere.SetPhiResolution(30)
        sphere.SetRadius(2.0)
        sphereMapper = vtk.vtkPolyDataMapper()
        sphereMapper.SetInputConnection(sphere.GetOutputPort())
        self.highlighter = vtk.vtkActor()
        self.highlighter.SetMapper(sphereMapper)
        self.highlighter.GetProperty().SetColor(0.5,0.8,0.1)
        self.highlighter.GetProperty().SetOpacity(0)
        self._ren.AddActor(self.highlighter)
        
        self.modeText = vtk.vtkTextActor()        
        self.modeText.SetDisplayPosition(10, 960)
        self.modeText.SetInput("Measuring Mode")
        self._ren.AddActor(self.modeText)  
        
    def highlightPosition(self, position):
        self.highlighter.SetPosition(position)
        self.highlighter.GetProperty().SetOpacity(1.0)
       
    def cameraAnyEvent(self,obj,evt):
        self.cameraText.SetInput("Orientation X = %5.0f\nOrientation Y = %5.0f\nOrientation Z = %5.0f" % (obj.GetOrientation()[0],obj.GetOrientation()[1],obj.GetOrientation()[2]))
    
    def initAxes(self):
        self.axes = vtk.vtkAxesActor()
        self.axesWidget = vtk.vtkOrientationMarkerWidget()
        self.axes.AxisLabelsOff()
        self.axes.SetShaftTypeToCylinder()
        
        self.axesWidget.SetOrientationMarker(self.axes)
        self.axesWidget.SetInteractor(self._iren)
        self.axesWidget.SetViewport(0.0, 0.0, 0.15, 0.15)
        self.axesWidget.SetEnabled(1)
        self.axesWidget.InteractiveOff()   
             
        self.cameraText = vtk.vtkTextActor()        
        self.cameraText.SetTextScaleModeToProp()
        self.cameraText.SetDisplayPosition(10, 10)
        
    def initBoxWidget(self):
        self.boxWidget = vtk.vtkBoxWidget()
        self.boxWidget.SetInteractor(self._iren)
        self.boxWidget.SetPlaceFactor(1.0)
        self.boxWidget.SetHandleSize(0.004)
        self.boxWidget.InsideOutOn()
        self.boxWidget.AddObserver("StartInteractionEvent", self.bwStartInteraction)
        self.boxWidget.AddObserver("InteractionEvent", self.bwClipVolumeRender)
        self.boxWidget.AddObserver("EndInteractionEvent", self.bwEndInteraction) 
        self.planes = vtk.vtkPlanes()
                
    def initPlaneWidget(self):
        self.planeWidget = [vtk.vtkImplicitPlaneWidget() for i in range(6)]
        self.pwPlane = [vtk.vtkPlane() for i in range(6)]
        self.pwClippingPlanes = vtk.vtkPlaneCollection()
        
#         HSV = [(x * 1.0 / nvol, 0.8, 0.8) for x in range(nvol)]
#         RGB = map(lambda x: colorsys.hsv_to_rgb(*x), HSV)        
        
        for i in range(6):
            self.planeWidget[i].SetInteractor(self._iren)
            self.planeWidget[i].SetPlaceFactor(1.1)
            self.planeWidget[i].TubingOff()
            self.planeWidget[i].DrawPlaneOff()
            self.planeWidget[i].OutsideBoundsOff()  
            self.planeWidget[i].OutlineTranslationOff() 
            self.planeWidget[i].ScaleEnabledOff()
            self.planeWidget[i].SetHandleSize(0.25*self.planeWidget[i].GetHandleSize())
            self.planeWidget[i].SetKeyPressActivationValue(str(i+1))
            self.planeWidget[i].SetInteractor(self._iren)
            self.planeWidget[i].AddObserver("InteractionEvent", self.pwCallback)  
            self.pwClippingPlanes.AddItem(self.pwPlane[i])
            
    def initSphereWidget(self):
#         self.sphereWidget = vtk.vtkSphereWidget()
#         self.sphereWidget.SetInteractor(self._iren)
#         
#         self.sphereWidget.TranslationOff()
#         self.sphereWidget.ScaleOff()
#         self.sphereWidget.HandleVisibilityOn()        
# #         self.sphereWidget.EnabledOn()  
#         self.sphereWidget.SetKeyPressActivationValue ("l")
#         self.sphereWidget.AddObserver("InteractionEvent", self.sphereWidgetMoveLight)        
        
#         self.light = vtk.vtkLight()
#         self.light.SetLightTypeToHeadlight()
#         self.light.PositionalOn()
#         self._ren.AddLight(self.light)   
        
        self.lightkit = vtk.vtkLightKit()
        self.lightkit.SetKeyLightIntensity(2.0)
        self.lightkit.MaintainLuminanceOn()
        
#         print self.lightkit.GetBackLightWarmth(), self.lightkit.GetFillLightWarmth(), self.lightkit.GetHeadLightWarmth()
        
        self.lightkit.AddLightsToRenderer(self._ren)
        
    def color_picker(self, ncolors, sat_ = 0.8, val_ = 0.8):
        HSV = [(x * 1.0 / ncolors, sat_, val_) for x in range(ncolors)]
        return map(lambda x: colorsys.hsv_to_rgb(*x), HSV)        
        
    def initLabels(self):
        nlabels = self.labelControlItems.combobox_labels.count()
        self.labelText = [vtk.vtkVectorText() for i in range(nlabels)]
        self.labelMapper = [vtk.vtkPolyDataMapper() for i in range(nlabels)]
        self.labelActor = [vtk.vtkFollower() for i in range(nlabels)]
        self.labelLine = [vtk.vtkLineWidget2() for i in range(nlabels)]
        self.labelLineRep = [vtk.vtkLineRepresentation() for i in range(nlabels)]
        rgb_set = self.color_picker(nlabels)

        
        for i in range(nlabels):            
            self.labelText[i].SetText("Label"+str(i+1))
            
            self.labelMapper[i].SetInputConnection(self.labelText[i].GetOutputPort())
            
            self.labelActor[i].SetMapper(self.labelMapper[i])
            self.labelActor[i].SetScale(2.0)            
            self.labelActor[i].GetProperty().SetColor(rgb_set[i][0],rgb_set[i][1],rgb_set[i][2]) 
            
            self.labelLineRep[i].SetHandleSize(5)
            self.labelLineRep[i].GetPoint2Representation().GetProperty().SetOpacity(0.0)
            self.labelLineRep[i].GetPoint1Representation().GetProperty().SetColor(rgb_set[i][0],rgb_set[i][1],rgb_set[i][2])
            self.labelLineRep[i].GetLineProperty().SetColor(rgb_set[i][0],rgb_set[i][1],rgb_set[i][2])
            
            self.labelLine[i].SetRepresentation(self.labelLineRep[i])            
            self.labelLine[i].SetInteractor(self._iren)
            self.labelLine[i].AddObserver("StartInteractionEvent", self.labelBeginInteraction)
            self.labelLine[i].AddObserver("InteractionEvent", self.labelEndInteraction)        
            
            
            self.labelActor[i].SetCamera(self._ren.GetActiveCamera())
           
    def labelBeginInteraction(self, obj,evt):
        nlabels = self.labelControlItems.combobox_labels.count()
        for i in range(nlabels):
            self.labelActor[i].SetPosition(self.labelLine[i].GetLineRepresentation().GetPoint2WorldPosition())
        
    def labelEndInteraction(self, obj,evt):
        nlabels = self.labelControlItems.combobox_labels.count()
        self.labelCallback()
        for i in range(nlabels):
            self.labelActor[i].SetPosition(self.labelLine[i].GetLineRepresentation().GetPoint2WorldPosition())
            
    def resetAllPlaneWidgets(self):
        if self.reader:
            xmin, xmax, ymin, ymax, zmin, zmax = self.reader.GetOutput().GetBounds()
                
            self.planeWidget[0].SetOrigin(0,ymax/2.,zmax/2.)
            self.planeWidget[0].SetNormal(1,0,0)
            self.planeWidget[0].UpdatePlacement() 
    
            self.planeWidget[1].SetOrigin(xmax/2.,0,zmax/2.)
            self.planeWidget[1].SetNormal(0,1,0)
            self.planeWidget[1].UpdatePlacement() 
    
            self.planeWidget[2].SetOrigin(xmax/2.,ymax/2.,0)
            self.planeWidget[2].SetNormal(0,0,1)
            self.planeWidget[2].UpdatePlacement() 
            
    
            self.planeWidget[3].SetOrigin(xmax,ymax/2.,zmax/2.)
            self.planeWidget[3].SetNormal(-1,0,0)
            self.planeWidget[3].UpdatePlacement() 
    
            self.planeWidget[4].SetOrigin(xmax/2.,ymax,zmax/2.)
            self.planeWidget[4].SetNormal(0,-1,0)
            self.planeWidget[4].UpdatePlacement() 
    
            self.planeWidget[5].SetOrigin(xmax/2.,ymax/2.,zmax)
            self.planeWidget[5].SetNormal(0,0,-1)
            self.planeWidget[5].UpdatePlacement() 
                
            self.pwCallback(self, None)
            self._renWin.Render()
            
    def resetPlaneWidget(self):
        if self.reader:
            
            sendername = self.sender().objectName()            
            
            xmin, xmax, ymin, ymax, zmin, zmax = self.reader.GetOutput().GetBounds()
            
            if sendername == "resetplane0":                
                self.planeWidget[0].SetOrigin(0,ymax/2.,zmax/2.)
                self.planeWidget[0].SetNormal(1,0,0)
                self.planeWidget[0].UpdatePlacement() 
            elif sendername == "resetplane1":
                self.planeWidget[1].SetOrigin(xmax/2.,0,zmax/2.)
                self.planeWidget[1].SetNormal(0,1,0)
                self.planeWidget[1].UpdatePlacement() 
            elif sendername == "resetplane2":        
                self.planeWidget[2].SetOrigin(xmax/2.,ymax/2.,0)
                self.planeWidget[2].SetNormal(0,0,1)
                self.planeWidget[2].UpdatePlacement() 
            elif sendername == "resetplane3":
                self.planeWidget[3].SetOrigin(xmax,ymax/2.,zmax/2.)
                self.planeWidget[3].SetNormal(-1,0,0)
                self.planeWidget[3].UpdatePlacement() 
            elif sendername == "resetplane4":
                self.planeWidget[4].SetOrigin(xmax/2.,ymax,zmax/2.)
                self.planeWidget[4].SetNormal(0,-1,0)
                self.planeWidget[4].UpdatePlacement() 
            elif sendername == "resetplane5":        
                self.planeWidget[5].SetOrigin(xmax/2.,ymax/2.,zmax)
                self.planeWidget[5].SetNormal(0,0,-1)
                self.planeWidget[5].UpdatePlacement() 
                
            self.pwCallback(self, None)
            self._renWin.Render()            
           
    def displayCameraOrientation(self):
        if self.button_cameratext.isChecked():
            self._ren.AddActor(self.cameraText)
        else:
            self._ren.RemoveActor(self.cameraText)
            
        self._renWin.Render()
            
    def pwCallback(self, obj, evt):
        self.volumeMapper.RemoveAllClippingPlanes()
        self.pwClippingPlanes.RemoveAllItems()

        for i in range(6):
            self.planeWidget[i].GetPlane(self.pwPlane[i])
            self.pwClippingPlanes.AddItem(self.pwPlane[i])   
            
        self.volumeMapper.SetClippingPlanes(self.pwClippingPlanes) 
        
    def initMeasurementTool(self):
        self.distanceScale = 1
        self.distanceRep = vtk.vtkDistanceRepresentation3D()
        self.distanceRep.SetLabelFormat("%-#6.3g mm")
        self.distanceRep.GetLineProperty().SetColor(0.,0.2,1.0)
        self.distanceRep.GetLabelActor().GetProperty().SetOpacity(0.0)
                
        self.distanceWidget = vtk.vtkDistanceWidget()
        self.distanceWidget.SetInteractor(self._iren)
        self.distanceWidget.SetWidgetStateToManipulate()
        self.distanceWidget.CreateDefaultRepresentation()
        self.distanceWidget.SetRepresentation(self.distanceRep)

        
        self.distanceWidget.SetEnabled(0)  
#         self.removeAllMouseEvents(self.distanceWidget)
        
        self.distanceText = vtk.vtkTextActor()        
        self.distanceText.SetDisplayPosition(900, 10)
        self.distanceText.SetInput("distance = ")     

    def initAngleMeasurementTool(self):
        self.angleRep = vtk.vtkAngleRepresentation3D()
#         self.angleRep.SetLabelFormat("%-#6.3g degrees")
        
        self.angleWidget = vtk.vtkAngleWidget()
        self.angleWidget.SetInteractor(self._iren)
        self.angleWidget.SetWidgetStateToManipulate()
        self.angleWidget.CreateDefaultRepresentation()
        self.angleWidget.SetRepresentation(self.angleRep)
        self.angleWidget.SetEnabled(0)
        
        self.angleWidget.AddObserver("StartInteractionEvent", self.awStartInteraction)
        self.angleWidget.AddObserver("InteractionEvent", self.awUpdateMeasurement)
        self.angleWidget.AddObserver("EndInteractionEvent", self.awEndInteraction)   

        self.angleText = vtk.vtkTextActor()        
#         self.angleText.SetTextScaleModeToProp()
        self.angleText.SetDisplayPosition(900, 20)  

        self.angleText.SetInput("angle = ")           
           
    def dwStartInteraction(self, obj, event):
        self.distanceRep.GetPoint1WorldPosition(self.last_distw_p1)
        self.distanceRep.GetPoint2WorldPosition(self.last_distw_p2)
        self.labelLineRep[0].GetPoint1WorldPosition(self.label1_pt)
        self.labelLineRep[1].GetPoint1WorldPosition(self.label2_pt)  
        self.label_sqr_dist_thresh = 1e-4*vtk.vtkMath.Distance2BetweenPoints(self.label1_pt, self.label2_pt)
        
        self._renWin.SetDesiredUpdateRate(10)
     
    def dwEndInteraction(self, obj, event):
        self.distanceRep.GetPoint1Representation().GetSelectedProperty().SetColor(0.0,1.0,0)
        self.distanceText.SetInput("distance = %-#6.3g mm" % (self.distanceRep.GetDistance()/ self.distanceScale))        
        self._renWin.SetDesiredUpdateRate(0.001)        
         
    def dwUpdateMeasurement(self, obj, event):
        self.distanceRep.GetPoint1WorldPosition(self.distw_p1)
        self.distanceRep.GetPoint2WorldPosition(self.distw_p2)
        
        if vtk.vtkMath.Distance2BetweenPoints(self.distw_p1, self.last_distw_p1) > vtk.vtkMath.Distance2BetweenPoints(self.distw_p2, self.last_distw_p2):
            pt = self.distw_p1
        else:
            pt = self.distw_p2
            
        label_dist = self.findLabelPointDistance(pt)              
        if label_dist < self.label_sqr_dist_thresh:
            self.distanceRep.GetPoint1Representation().GetSelectedProperty().SetColor(1.0,0.0,0)
        else:
            self.distanceRep.GetPoint1Representation().GetSelectedProperty().SetColor(0.0,1.0,0)
            
        if self.findLabelPointDistance(self.distw_p1) < self.label_sqr_dist_thresh and self.findLabelPointDistance(self.distw_p2) < self.label_sqr_dist_thresh:
            self.distanceRep.GetPoint1Representation().GetProperty().SetColor(1.,0.,0.)
            self.distanceRep.GetLabelProperty().SetColor(1.,0.,0.)
            self.distanceRep.GetLineProperty().SetColor(1.,1.,0.)
            self.distanceRep.GetGlyphActor().GetProperty().SetColor(1.,0.,0.)
        else:
            self.distanceRep.GetPoint1Representation().GetProperty().SetColor(1.,1.,1.)
            self.distanceRep.GetLabelProperty().SetColor(1.,1.,1.)
            self.distanceRep.GetLineProperty().SetColor(0.,0.,1.)
            self.distanceRep.GetGlyphActor().GetProperty().SetColor(1.,1.,1.)

        self.distanceText.SetInput("distance = %-#6.3g mm" % (self.distanceRep.GetDistance() / self.distanceScale))  

    def awStartInteraction(self, obj, event):
        self._renWin.SetDesiredUpdateRate(10)
    
    def awEndInteraction(self, obj, event):
        self.angleText.SetInput("angle = %-#6.3g degrees" % vtk.vtkMath.DegreesFromRadians(obj.GetAngleRepresentation().GetAngle()))        
        self._renWin.SetDesiredUpdateRate(0.001)        
        
    def awUpdateMeasurement(self, obj, event):
#         print obj.GetDistanceRepresentation().GetDistance()
        self.angleText.SetInput("angle = %-#6.3g degrees" % vtk.vtkMath.DegreesFromRadians(obj.GetAngleRepresentation().GetAngle()))     

    def getVolumeMapper(self):
        return self.volumeMapper
        
    def getRendererWindow(self):
        return self._renWin

    def getRenderer(self):
        return self._ren
    
    def getRendererWindowInteractor(self):
        return self._iren
    
    def setReader(self,reader):
        self.reader = reader
        
    def setVolumeDimension(self,dim):
        self.dim = dim
        
    def setVolumeSpacing(self,spacing):
        self.spacing = spacing
        
    def setVolumeMapper(self,volumeMapper):
        self.volumeMapper = volumeMapper
              
    def bwStartInteraction(self, obj, event):
        self._renWin.SetDesiredUpdateRate(10)
    
    def bwEndInteraction(self, obj, event):
        self._renWin.SetDesiredUpdateRate(0.001)
    
    def bwClipVolumeRender(self, obj, event):
        obj.GetPlanes(self.planes)
        self.volumeMapper.SetClippingPlanes(self.planes)  
        
    def read_Philips_echo_volume(self,filename):
        header = dicom.read_file(filename)
        spacing_x, spacing_y, spacing_z = header.PhysicalDeltaX, header.PhysicalDeltaY, header[0x3001, 0x1003].value
        dim_x, dim_y, dim_z, number_of_frames = header.Columns, header.Rows, header[0x3001, 0x1001].value, header.NumberOfFrames
        raw = np.fromstring(header.PixelData, dtype=np.uint8)
        return np.reshape(raw, (dim_x, dim_y, dim_z, number_of_frames), order="F"), (10*spacing_x, 10*spacing_y, 10*spacing_z), header.SOPInstanceUID
    
    def loadEcho(self):        
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        self.fname = QFileDialog.getOpenFileName(self,"Select DICOM File",initfdir,"All Files (*);;Text Files (*.txt)", "", options)        
        
        if self.fname:
            
            self.volT = []
            self.cb = []            
            
            self.volT, self.spacing, self.sopuid = self.read_Philips_echo_volume(str(self.fname))
            self.dim = self.volT.shape
            
            vol = self.volT[:, :, :, 0]
            
            self.reader = vtk.vtkImageImport()
            data_string = vol.flatten("A")
            self.reader.CopyImportVoidPointer(data_string, len(data_string))
            self.reader.SetDataScalarTypeToUnsignedChar()
            self.reader.SetNumberOfScalarComponents(1)
            self.reader.SetDataExtent(0, self.dim[0] - 1, 0, self.dim[1] - 1, 0, self.dim[2] - 1)
            self.reader.SetWholeExtent(0, self.dim[0] - 1, 0, self.dim[1] - 1, 0, self.dim[2] - 1)
            self.reader.SetDataSpacing(self.spacing[0], self.spacing[1], self.spacing[2]) 
#             self.reader.SetDataOrigin((self.dim[0] - 1)*self.spacing[0]/2.0, (self.dim[1] - 1)*self.spacing[1]/2.0, (self.dim[2] - 1)*self.spacing[2]/2.0)
            
 
            self.volumeMapper = vtk.vtkOpenGLVolumeTextureMapper3D()
            self.volumeMapper.SetPreferredMethodToNVidia()
            self.volumeMapper.SetSampleDistance(0.5)
            
#             self.volumeMapper = vtk.vtkSmartVolumeMapper()
#             self.volumeMapper = vtk.vtkOpenGLGPUVolumeRayCastMapper()
            self.volume.SetMapper(self.volumeMapper)  
            
            self.imageGaussianSmooth.SetInputConnection(self.reader.GetOutputPort()) 
            self.imageGaussianSmooth.Update()
                    
            self.volumeMapper.SetInputConnection(self.imageGaussianSmooth.GetOutputPort())
    
            self.loadData()
            
            self._ren.AutomaticLightCreationOff()
            
            self.isplay = False
            self.isrotate = False
            
            self.slider_imageNumber.setEnabled(True)
            self.button_iterate.setEnabled(True)             
             
            self.slider_imageNumber.setMaximum(self.volT.shape[3]-1)
            self.slider_imageNumber.setValue(0)
            self.slider_imageNumber_valuechanged()
            self.cb = vtkTimerCallBack(self.volT.shape[3], self.isplay, self.isrotate, self.slider_imageNumber)
            
            self.cb.renderer = self._ren
    
    def loadDir(self, dirname=None):
        if not dirname:
            options = QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly | QFileDialog.DontUseNativeDialog
            self.dirname = str(QFileDialog.getExistingDirectory(self,"Select DICOM Directory", initddir, options))
        else:
            self.dirname = dirname
            
        if self.dirname:
            subdir = [name for name in os.listdir(self.dirname) if os.path.isdir(os.path.join(self.dirname, name))]            
            while subdir and self.dirname:
                self.dirname = str(QFileDialog.getExistingDirectory(self, "Select DICOM Directory", self.dirname, options))
                if (self.dirname):
                    subdir = [name for name in os.listdir(self.dirname) if os.path.isdir(os.path.join(self.dirname, name))]        
                
        if self.dirname:
            self._iren.RemoveObservers('TimerEvent')
            
            self.volT = []
            self.cb = []            
            
            self.reader = vtk.vtkDICOMImageReader()
            self.reader.SetDirectoryName(self.dirname)
            self.reader.Update()
            
            self.spacing, self.sopuid = self.reader.GetOutput().GetSpacing(), self.reader.GetStudyUID()
            self.dim = self.reader.GetOutput().GetDimensions()           


            
#             self.volumeMapper = vtk.vtkFixedPointVolumeRayCastMapper()
#             self.volumeMapper = vtk.vtkOpenGLGPUVolumeRayCastMapper()
            
            self.volumeMapper = vtk.vtkVolumeTextureMapper3D()

#             self.volumeMapper = vtk.vtkOpenGLVolumeTextureMapper3D()
#             self.volumeMapper.SetPreferredMethodToFragmentProgram ()
            self.volumeMapper.SetPreferredMethodToNVidia()
            self.volumeMapper.SetSampleDistance(0.5)

#             if self.reader.GetDataScalarType() == 4:
#                 self.volumeMapper = vtk.vtkOpenGLVolumeTextureMapper3D()
#             else:                        
#                 self.volumeMapper = vtk.vtkOpenGLVolumeTextureMapper3D()


            self.volume.SetMapper(self.volumeMapper)  
            
            self.imageGaussianSmooth.SetInputConnection(self.reader.GetOutputPort()) 
            self.imageGaussianSmooth.Update()
                    
            self.volumeMapper.SetInputConnection(self.imageGaussianSmooth.GetOutputPort())
            
            
#             imcast = vtk.vtkImageCast()
#             imcast.SetOutputScalarTypeToUnsignedShort()
#             imcast.SetInputConnection(self.imageGaussianSmooth.GetOutputPort())
#             self.volumeMapper.SetInputConnection(imcast.GetOutputPort())
            
            self.slider_imageNumber.setEnabled(False)
            self.slider_imageNumber.setValue(0)
            self.button_iterate.setEnabled(False)  
            
            self.dirtext.SetInput(self.dirname)
            
            self.loadData()
               
    def resetBoxWidget(self):
        self.volumeMapper.RemoveAllClippingPlanes()
        self.boxWidget.PlaceWidget()
        
    def VTKtoNP4x4(self, vtkMat):
        r = np.zeros((4,4))
        for x,y in [(x,y) for x in range(4) for y in range(4)]:
            r[x][y] = vtkMat.GetElement(x,y)
        return r
    
    def afterInit(self):

        self.dwCallback()
        self.labelCallback()
        self.volume.AddObserver(vtk.vtkCommand.AnyEvent, self.volumeCallback) 
    
    #called when volume changes, saves dw and labelLine relative position
    def volumeCallback(self, obj, event, userData = None):

        self.dwUpdate()
        self.labelUpdate()
        
    #called when dw changes, saves relative position
    def dwCallback(self):

        dwLocation1A = np.empty((3))
        dwLocation2A = np.empty((3))
        self.dwLocation1R = np.empty((3))
        self.dwLocation2R = np.empty((3))
        self.distanceWidget.GetDistanceRepresentation().GetPoint1WorldPosition(dwLocation1A)
        self.distanceWidget.GetDistanceRepresentation().GetPoint2WorldPosition(dwLocation2A)
        transform = vtk.vtkTransform()
        transform.Concatenate(self.volume.GetMatrix())
        transform.GetInverse().TransformPoint(dwLocation1A, self.dwLocation1R)
        transform.GetInverse().TransformPoint(dwLocation2A, self.dwLocation2R)
    
    #called when volume changes, sets dw absolute position based on relative position
    def dwUpdate(self):

        transform = vtk.vtkTransform()
        transform.Concatenate(self.volume.GetMatrix())
        dwLocation1A = np.empty((3))
        dwLocation2A = np.empty((3))
        transform.TransformPoint(self.dwLocation1R, dwLocation1A)
        transform.TransformPoint(self.dwLocation2R, dwLocation2A)
        self.distanceWidget.GetDistanceRepresentation().SetPoint1WorldPosition(dwLocation1A)
        self.distanceWidget.GetDistanceRepresentation().SetPoint2WorldPosition(dwLocation2A)
        
    #called when labelLine changes, saves relative position
    def labelCallback(self):

        transform = vtk.vtkTransform()
        transform.Concatenate(self.volume.GetMatrix())
        nlabels = self.labelControlItems.combobox_labels.count()
        labelLocation1A = [np.empty((3)) for i in range(nlabels)]
        labelLocation2A = [np.empty((3)) for i in range(nlabels)]
        labelLocation3A = [np.empty((3)) for i in range(nlabels)]
        self.labelLocation1R = [np.empty((3)) for i in range(nlabels)]
        self.labelLocation2R = [np.empty((3)) for i in range(nlabels)]
        self.labelLocation3R = [np.empty((3)) for i in range(nlabels)]
        for i in range(nlabels):
            for j in range(3):
                labelLocation1A[i][j] = self.labelLine[i].GetLineRepresentation().GetPoint1WorldPosition()[j]
                labelLocation2A[i][j] = self.labelLine[i].GetLineRepresentation().GetPoint2WorldPosition()[j]
                labelLocation3A[i][j] = self.labelActor[i].GetPosition()[j]
            transform.GetInverse().TransformPoint(labelLocation1A[i], self.labelLocation1R[i])
            transform.GetInverse().TransformPoint(labelLocation2A[i], self.labelLocation2R[i])
            transform.GetInverse().TransformPoint(labelLocation3A[i], self.labelLocation3R[i])
        
    #called when volume changes, sets labelLine absolute position based on relative position
    def labelUpdate(self):

        transform = vtk.vtkTransform()
        transform.Concatenate(self.volume.GetMatrix())
        nlabels = self.labelControlItems.combobox_labels.count()
        labelLocation1A = [np.empty((3)) for i in range(nlabels)]
        labelLocation2A = [np.empty((3)) for i in range(nlabels)]
        labelLocation3A = [np.empty((3)) for i in range(nlabels)]
        for i in range(nlabels):
            transform.TransformPoint(self.labelLocation1R[i], labelLocation1A[i])
            transform.TransformPoint(self.labelLocation2R[i], labelLocation2A[i])
            transform.TransformPoint(self.labelLocation3R[i], labelLocation3A[i])
            self.labelLine[i].GetLineRepresentation().SetPoint1WorldPosition(labelLocation1A[i])
            self.labelLine[i].GetLineRepresentation().SetPoint2WorldPosition(labelLocation2A[i])
            self.labelActor[i].SetPosition(labelLocation3A[i])
        self.labelBeginInteraction(None, None)
    
    def loadData(self):
#         self._ren.GetActors().RemoveAllItems()
#         self._ren.GetVolumes().RemoveAllItems()
        self._ren.AddVolume(self.volume)
        self.volume.SetMapper(self.volumeMapper)
        self.volume.SetProperty(self.volumeProperty)   
                        
#         if vtk.VTK_VERSION < '6':
#             self.boxWidget.SetInput(self.reader.GetOutput())
#         else:
#             self.boxWidget.SetInputConnection(self.reader.GetOutputPort())
#             
#         for i in range(6):
#             if vtk.VTK_VERSION < '6':
#                 self.planeWidget[i].SetInput(self.reader.GetOutput())
#             else:
#                 self.planeWidget[i].SetInputConnection(self.reader.GetOutputPort())
#                 
#             self.planeWidget[i].PlaceWidget()
#             self.planeWidget[i].SetInteractor(self._iren)
#             self.removeAllMouseEvents(self.planeWidget[i])
#             
#                         
#                         
#         self.resetBoxWidget()   
#         self.resetAllPlaneWidgets()     
                
        self.create_color_opacity_table(tfuncdir + str(self.transferFunctionControlItems.combobox_transfunction.itemText(self.transferFunctionControlItems.combobox_transfunction.currentIndex())))
        
        for scale, idim, val in zip((self.scale_xmin,self.scale_ymin,self.scale_zmin,self.scale_xmax,self.scale_ymax,self.scale_zmax),(0,1,2,0,1,2),(0,0,0,self.dim[0],self.dim[1],self.dim[2])):
            scale.setMaximum(self.dim[idim])
            scale.setMinimum(0)
            scale.setValue(val)
            scale.setEnabled(True)
    
        for comp in (self.button_resetcrop, self.button_box):
            comp.setEnabled(True)
            
        for i in range(6):
            self.planeWidgetControlItems.button_pwidgets[i].setEnabled(True)
            self.planeWidgetControlItems.button_pwidgetreset[i].setEnabled(True)
            
        self.planeWidgetControlItems.button_pwdigetresetall.setEnabled(True)
            
        self.combobox_loadsettings.clear()            
        setting_files = glob.glob1(settings_dir+os.sep+self.sopuid, "*.xml")
        if setting_files:
            self.combobox_loadsettings.addItems(setting_files)  
            self.combobox_loadsettings.setCurrentIndex(self.combobox_loadsettings.findText("last-settings.xml"))
                        

        self.lightingControlItems.button_shade.setEnabled(True)
        self.lightingControlItems.button_interpolation.setEnabled(True)
        self.lightingControlItems.button_gradientopacity.setEnabled(True)
        
        for i in range(self.labelControlItems.combobox_labels.count()):
            self.labelLineRep[i].SetPoint1WorldPosition(self.reader.GetOutput().GetCenter())
            self.labelLineRep[i].SetPoint2WorldPosition(self.reader.GetOutput().GetOrigin())
        
        self.labelControlItems.button_label.setChecked(False)
                            
        self.smoothVolume()
        self.volumeMapper.CroppingOff()
        if self.firstRender:                
            self._ren.ResetCamera()     
            self._ren.ResetCameraClippingRange() 
            self.firstRender = False
        self.initStylus()
        self.dwPlaced = False
        self.firstExecute = True
        
        self._iren.RemoveObservers('MiddleButtonPressEvent')
        self._iren.RemoveObservers('MiddleButtonReleaseEvent')
        
        
        self.headtrack = vtkTimerHeadTrack(self.cam, self.lineActor, self.volume, self)
        self.headtrack.renderer = self._ren
        self.tag_headtrack = self._iren.AddObserver('TimerEvent', self.headtrack.execute)
        self._iren.CreateRepeatingTimer(20)
        
        self._renWin.Render()
#         self.volume.AddObserver(vtk.vtkCommand.AnyEvent, self.volumeCallback)     
        self.time_rendered = time.time()
                
    def create_color_opacity_table(self,fname):    
        tree = ET.parse(fname)
    
        colorFunc = vtk.vtkColorTransferFunction()
        scalaropacityFunc = vtk.vtkPiecewiseFunction()
        gradientopacityFunc = vtk.vtkPiecewiseFunction()    
        
        rgbbranch = tree.iter('RGBTransferFunction')
        rgbsubbranch = rgbbranch.next().iter('ColorTransferFunction')
        for elem in rgbsubbranch.next():
            rgbval = elem.get('Value').split(" ")
            colorFunc.AddRGBPoint(float(elem.get('X')), float(rgbval[0]), float(rgbval[1]), float(rgbval[2]), float(elem.get('MidPoint')), float(elem.get('Sharpness')))    
    
        scalaropacitybranch = tree.iter('ScalarOpacity')
        scalaropacitysubbranch = scalaropacitybranch.next().iter('PiecewiseFunction')
        for elem in scalaropacitysubbranch.next():
            scalaropacityFunc.AddPoint(float(elem.get('X')), float(elem.get('Value')), float(elem.get('MidPoint')), float(elem.get('Sharpness')))
    
        gradientopacitybranch = tree.iter('GradientOpacity')
        gradientopacitysubbranch = gradientopacitybranch.next().iter('PiecewiseFunction')
        for elem in gradientopacitysubbranch.next():
            gradientopacityFunc.AddPoint(float(elem.get('X')), float(elem.get('Value')), float(elem.get('MidPoint')), float(elem.get('Sharpness')))
    
        volumeProp = tree.iter("VolumeProperty").next()
        component = tree.iter('Component').next()
        
#         self.volumeProperty.SetShade(int(component.get('Shade')))
#         self.volumeProperty.SetAmbient(float(component.get('Ambient')))
#         self.volumeProperty.SetDiffuse(float(component.get('Diffuse')))
#         self.volumeProperty.SetSpecular(float(component.get('Specular')))
        self.volumeProperty.SetSpecularPower(float(component.get('SpecularPower')))
    #   self.  volumeProperty.SetColorChannels(component.get('ColorChannels'))
        self.volumeProperty.SetDisableGradientOpacity(int(component.get('DisableGradientOpacity')))
    #   self.  volumeProperty.SetComponentWeight(component.get('ComponentWeight'))
        self.volumeProperty.SetScalarOpacityUnitDistance(float(component.get('ScalarOpacityUnitDistance')))
        self.volumeProperty.SetScalarOpacity(scalaropacityFunc)
        self.volumeProperty.SetColor(colorFunc)
        self.volumeProperty.SetGradientOpacity(gradientopacityFunc)    
        
        self.lightingControlItems.slider_ambient.setValue(100*float(component.get('Ambient')))  
        self.lightingControlItems.slider_diffuse.setValue(100*float(component.get('Diffuse')))
        self.lightingControlItems.slider_specular.setValue(100*float(component.get('Specular')))
        self.lightingControlItems.button_shade.setChecked(int(component.get('Shade')))
        self.lightingControlItems.button_interpolation.setChecked(int(volumeProp.get('InterpolationType')))
        
        self.adjustLights()
        self.setInterpolation()
        self.setShade()
        
    def setBoxWidget(self):
        if self.boxWidget.GetEnabled():
            self.boxWidget.Off()
        else:
            self.boxWidget.On()   
            
        self._renWin.Render() 
        
    def setPlaneWidgets(self):
        for i in range(6):
            if self.planeWidgetControlItems.button_pwidgets[i].isChecked():
                self.planeWidget[i].On()
                self.removeAllMouseEvents(self.planeWidget[i])
            else:
                self.planeWidget[i].Off()
            
                
    def updateTFunc(self):

        self.create_color_opacity_table(tfuncdir + str(self.transferFunctionControlItems.combobox_transfunction.itemText(self.transferFunctionControlItems.combobox_transfunction.currentIndex())))
        self._renWin.Render()

    def cropVolume(self):
        self.volumeMapper.SetCroppingRegionPlanes(self.scale_xmin.value()*self.spacing[0], self.scale_xmax.value()*self.spacing[0],\
                                                  self.scale_ymin.value()*self.spacing[1], self.scale_ymax.value()*self.spacing[1],\
                                                  self.scale_zmin.value()*self.spacing[2], self.scale_zmax.value()*self.spacing[2])
        self.volumeMapper.CroppingOn()
        self._renWin.Render()

    def resetCrop(self):
        for scale, val in zip((self.scale_xmin,self.scale_ymin,self.scale_zmin,self.scale_xmax,self.scale_ymax,self.scale_zmax),(0,0,0,self.dim[0],self.dim[1],self.dim[2])):
            scale.setValue(val)
        
        self.cropVolume()
        self.resetBoxWidget()
        self._renWin.Render()

    def setStereo(self):
        if self._renWin.GetStereoRender():
            self._renWin.StereoRenderOff()
        else:
            self._renWin.StereoRenderOn()
            
        self._renWin.Render()
        
    def setShade(self):
        if self.lightingControlItems.button_shade.isChecked():
            self.volumeProperty.ShadeOn()
            self.lightingControlItems.slider_ambient.setValue(100*self.volumeProperty.GetAmbient())
            self.lightingControlItems.slider_diffuse.setValue(100*self.volumeProperty.GetDiffuse())
            self.lightingControlItems.slider_specular.setValue(100*self.volumeProperty.GetSpecular())
            
            for comp in (self.lightingControlItems.slider_ambient, self.lightingControlItems.slider_diffuse, self.lightingControlItems.slider_specular, self.lightingControlItems.slider_keylightintensity):
                comp.setEnabled(True)
            
        else:
            self.volumeProperty.ShadeOff()
            
        self._renWin.Render()
        
    def setInterpolation(self):
        if self.lightingControlItems.button_interpolation.isChecked():
            self.volumeProperty.SetInterpolationTypeToLinear()
        else:
            self.volumeProperty.SetInterpolationTypeToNearest()
                        
        self._renWin.Render()
        
    def setDisableGradientOpacity(self):
        if self.lightingControlItems.button_gradientopacity.isChecked():
            self.volumeProperty.DisableGradientOpacityOn()
        else:
            self.volumeProperty.DisableGradientOpacityOff()
            
        self._renWin.Render()

    def setMeasurement(self):
        nlabels = self.labelControlItems.combobox_labels.count()
        if self.distanceWidget.GetEnabled(): 
            self.distanceWidget.EnabledOff()
            self._ren.RemoveActor(self.distanceText) 
        else:
            for i in range(nlabels):
                self.labelLine[i].ProcessEventsOff()
            
            self.distanceWidget.EnabledOn()
            self._ren.AddActor(self.distanceText) 
        
        self._renWin.Render()

    def setAngleMeasurement(self):
        if self.angleWidget.GetEnabled(): 
            self.angleWidget.EnabledOff()
            self._ren.RemoveActor(self.angleText) 
        else:
            self.angleWidget.EnabledOn()
            self._ren.AddActor(self.angleText) 
        
        self._renWin.Render()

    def saveSettings(self):
        
        if self.sopuid:
#             self.pwClippingPlanes.GetMTime()
                
            root = ET.Element("root")    
            sliders = ET.SubElement(root, "sliders")
                
            for sldr, txt in zip((self.scale_xmin,self.scale_ymin,self.scale_zmin,self.scale_xmax,self.scale_ymax,self.scale_zmax),('x_min_slider', 'y_min_slider', 'z_min_slider', 'x_max_slider', 'y_max_slider', 'z_max_slider')):
                sliders.set(txt,str(sldr.value()))   
                
#             bwtransform = vtk.vtkTransform()
#             self.boxWidget.GetTransform(bwtransform)
            
#             bwtransformsettings = ET.SubElement(root, "boxwidgettransform")
#             buf = "" 
#             for i in range(4):
#                 for j in range(4):
#                     buf += str(bwtransform.GetMatrix().GetElement(i,j)) + ","
#         
#             bwtransformsettings.set("elements", buf[0:-1])
#             bwtransformsettings.set("mtime", str(self.boxWidget.GetMTime()))
                  
            camera = ET.SubElement(root, "camera")
            camera.set("Position", str(self._ren.GetActiveCamera().GetPosition()))
            camera.set("FocalPoint", str(self._ren.GetActiveCamera().GetFocalPoint()))
            camera.set("ViewUp", str(self._ren.GetActiveCamera().GetViewUp()))
            
            volume = ET.SubElement(root, "volume")
            volume.set("position", str(self.volume.GetPosition()))
            buf = ""
            for i in range(4):
                for j in range(4):
                    buf += str(self.volume.GetUserMatrix().GetElement(i,j)) + ","
            volume.set("userMatrix", buf[0:-1])
            volume.set("scale", str(self.volume.GetScale()))
            buf = ""
            for i in range(4):
                for j in range(4):
                    buf += str(self.volume.GetMatrix().GetElement(i,j)) + ","
            volume.set("Matrix", buf[0:-1])
            
#             planewidgetsettings = ET.SubElement(root, "planewidgetsettings")
#             planewidgetsettings.set("mtime", str(self.pwClippingPlanes.GetMTime()))
#             for i in range(6):
#                 planewidgetsettings.set("origin"+str(i), str(self.pwClippingPlanes.GetItem(i).GetOrigin()))
#                 planewidgetsettings.set("normal"+str(i), str(self.pwClippingPlanes.GetItem(i).GetNormal()))
            
            
            tfuncsettings = ET.SubElement(root, "tfunc")
            tfuncsettings.set("filename", str(self.transferFunctionControlItems.combobox_transfunction.itemText(self.transferFunctionControlItems.combobox_transfunction.currentIndex())))
            
            
            volPropSettings = ET.SubElement(root,'volumepropertysettings')
            rgbfuncsettings = ET.SubElement(volPropSettings,'rgbfuncsettings')
            rgbfuncsettings.set('NumberOfPoints',str(self.volumeProperty.GetRGBTransferFunction(0).GetSize()))
            val = np.empty((6))
            for i in range(self.volumeProperty.GetRGBTransferFunction(0).GetSize()):
                self.volumeProperty.GetRGBTransferFunction(0).GetNodeValue(i, val)
                rgbfuncsettings.set('pt'+str(i),"%d, %f, %f, %f, %f, %f" % (val[0],val[1],val[2],val[3],val[4],val[5]))
                
            scalarfuncsettings = ET.SubElement(volPropSettings,'scalarfuncsettings')        
            scalarfuncsettings.set('NumberOfPoints',str(self.volumeProperty.GetScalarOpacity(0).GetSize()))
            val = np.empty((4))
            for i in range(self.volumeProperty.GetScalarOpacity(0).GetSize()):    
                self.volumeProperty.GetScalarOpacity(0).GetNodeValue(i, val)
                scalarfuncsettings.set('pt'+str(i),"%d, %f, %f, %f" % (val[0],val[1],val[2],val[3]))
            
            gradientfuncsettings = ET.SubElement(volPropSettings,'gradientfuncsettings')        
            gradientfuncsettings.set('NumberOfPoints',str(self.volumeProperty.GetGradientOpacity(0).GetSize()))
            for i in range(self.volumeProperty.GetGradientOpacity(0).GetSize()):    
                self.volumeProperty.GetGradientOpacity(0).GetNodeValue(i, val)
                gradientfuncsettings.set('pt'+str(i),"%d, %f, %f, %f" % (val[0],val[1],val[2],val[3]))    
            
            
            labelsettings = ET.SubElement(root, 'labelsettings')
            labelsettings.set('NumberOfLabels', str(self.labelControlItems.combobox_labels.count()))             
            for i in range(self.labelControlItems.combobox_labels.count()):
                pos1, pos2 = self.labelLineRep[i].GetPoint1WorldPosition(), self.labelLineRep[i].GetPoint2WorldPosition()
#                 print pos1, pos2
                labelsettings.set('labelText'+str(i), '%s' % self.labelText[i].GetText())
                labelsettings.set('labelPos'+str(i), '%d, %f, %f, %f, %f, %f, %f' % (self.labelLine[i].GetEnabled(), pos1[0], pos1[1], pos1[2], pos2[0], pos2[1], pos2[2],))
                
            
            tree = ET.ElementTree(root)
            
            settings_subdir = self.sopuid            
            
            if not os.path.isdir(settings_dir):
                os.mkdir(settings_dir)
                
            if not os.path.isdir(settings_dir+os.sep+settings_subdir):
                os.mkdir(settings_dir+os.sep+settings_subdir)                
                
            if os.path.isfile(settings_dir + os.sep + settings_subdir+os.sep+"last-settings.xml"):
                os.rename(settings_dir + os.sep + settings_subdir+os.sep+"last-settings.xml", settings_dir + os.sep + settings_subdir+os.sep+datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")+".xml")
               
            
            options = QFileDialog.Options() | QFileDialog.DontUseNativeDialog
            filepath = QFileDialog.getSaveFileName(self,"Save Current Settings",settings_dir+ os.sep + settings_subdir+ os.sep + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"),"XML Files (*.xml)", "XML Files (*.xml)",options)
                
            if filepath:
                _, ext = os.path.splitext(str(filepath))
                if ext.upper() != '.XML':
                    filepath = '%s.xml' % filepath

                tree.write(filepath)  

                filedir, fname = os.path.split(str(filepath))
            
                self.combobox_loadsettings.clear()            
                setting_files = glob.glob1(settings_dir+os.sep+self.sopuid, "*.xml")
                if setting_files:
                    self.combobox_loadsettings.addItems(setting_files)   
                    self.combobox_loadsettings.setCurrentIndex(self.combobox_loadsettings.findText(fname+".xml"))
        
    def loadSettings(self):
        if self.sopuid:
            fname = self.sopuid
            tree = None

            if self.combobox_loadsettings.currentIndex() == -1:
                setting_fname = str(self.combobox_loadsettings.itemText(0))
            else:
                setting_fname = str(self.combobox_loadsettings.itemText(self.combobox_loadsettings.currentIndex()))
            
            if os.path.isfile(settings_dir + os.sep + fname+os.sep+setting_fname):
                tree = ET.parse(settings_dir + os.sep + fname+os.sep+setting_fname)  
            else:
                options = QFileDialog.Options() | QFileDialog.DontUseNativeDialog
                fname = QFileDialog.getOpenFileName(self,"Select Settings File",settings_dir,"All Files (*);;XML Files (*.xml)", "", options)
                if fname:
                    tree = ET.parse(fname)
            
            if tree:
                sliders = tree.iter('sliders').next()
             
                for sldr, txt in zip((self.scale_xmin,self.scale_ymin,self.scale_zmin,self.scale_xmax,self.scale_ymax,self.scale_zmax),('x_min_slider', 'y_min_slider', 'z_min_slider', 'x_max_slider', 'y_max_slider', 'z_max_slider')): 
                    sldr.setValue(int(sliders.get(txt)))
                    
                volume = tree.iter('volume').next()
                uMatrix = np.fromstring(volume.get("userMatrix"), sep=",")
                volTransform = vtk.vtkTransform()
                volMatrix = vtk.vtkMatrix4x4()
                for i in range(4):
                    for j in range(4):
                        volMatrix.SetElement(i, j, uMatrix[4*i + j])
                volTransform.SetMatrix(volMatrix)
                self.volume.SetUserTransform(volTransform)
                self.volume.SetPosition(np.fromstring(volume.get("position")[1:-1], sep=","))
                self.volume.SetScale(np.fromstring(volume.get("scale")[1:-1], sep=","))
                    
                    
#                 bwtransformsettings = tree.iter('boxwidgettransform').next()                    
#                 tmatrix = np.fromstring(bwtransformsettings.get("elements"), sep=",")
#                 
#                 bwtransform = vtk.vtkTransform()
#                 bwmatrix = vtk.vtkMatrix4x4()
#                 for i in range(4):
#                     for j in range(4):
#                         bwmatrix.SetElement(i,j,tmatrix[4*i+j])
#                      
#                 bwtransform.SetMatrix(bwmatrix)
#                 self.boxWidget.SetTransform(bwtransform)
#                 self.bwClipVolumeRender(self.boxWidget, None)
                
                
#                 if tree.findall('planewidgetsettings'):
#                     planewidgetsettings = tree.iter('planewidgetsettings').next()
#                     self.pwClippingPlanes.RemoveAllItems()
#                     for i in range(6):
#                         self.pwPlane[i].SetOrigin(np.fromstring(planewidgetsettings.get("origin"+str(i))[1:-1], sep=","))
#                         self.pwPlane[i].SetNormal(np.fromstring(planewidgetsettings.get("normal"+str(i))[1:-1], sep=","))
#                         self.pwClippingPlanes.AddItem(self.pwPlane[i])
#                         self.planeWidget[i].SetOrigin(self.pwPlane[i].GetOrigin()[0],self.pwPlane[i].GetOrigin()[1],self.pwPlane[i].GetOrigin()[2])
#                         self.planeWidget[i].SetNormal(self.pwPlane[i].GetNormal())
#                         self.planeWidget[i].UpdatePlacement()                    
                    
#                     if bwtransformsettings.get('mtime') and planewidgetsettings.get('mtime'):
#                         bwtime = int(bwtransformsettings.get('mtime'))
#                         pwtime = int(planewidgetsettings.get('mtime'))                                        
#                         if pwtime > bwtime:
#                             self.pwCallback(self, None)
                    

                camera = tree.iter('camera').next()
                self._ren.GetActiveCamera().SetPosition(np.fromstring(camera.get("Position")[1:-1], sep=","))
                self._ren.GetActiveCamera().SetFocalPoint(np.fromstring(camera.get("FocalPoint")[1:-1], sep=","))
                self._ren.GetActiveCamera().SetViewUp(np.fromstring(camera.get("ViewUp")[1:-1], sep=",")) 
                
                tfuncsetting = tree.iter("tfunc").next()
                self.transferFunctionControlItems.combobox_transfunction.setCurrentIndex(self.transferFunctionControlItems.combobox_transfunction.findText(tfuncsetting.get("filename")))
                self.create_color_opacity_table(tfuncdir + str(self.transferFunctionControlItems.combobox_transfunction.itemText(self.transferFunctionControlItems.combobox_transfunction.currentIndex())))
                
                rgbfunc = self.volumeProperty.GetRGBTransferFunction(0)
                rgbfunc.RemoveAllPoints()
                
                volumepropertysettings = tree.iter('volumepropertysettings').next()
                rgbfunctsettings = volumepropertysettings.iter('rgbfuncsettings').next()
                for i in range(int(rgbfunctsettings.get('NumberOfPoints'))):
                    val = np.fromstring(rgbfunctsettings.get('pt'+str(i)),sep=",")
                    rgbfunc.AddRGBPoint(val[0],val[1],val[2],val[3],val[4],val[5])
                    
                scalarfunc = self.volumeProperty.GetScalarOpacity(0)
                scalarfunc.RemoveAllPoints()            
        
                scalarfuncsettings = volumepropertysettings.iter('scalarfuncsettings').next()
                for i in range(int(scalarfuncsettings.get('NumberOfPoints'))):
                    val = np.fromstring(scalarfuncsettings.get('pt'+str(i)),sep=",")
                    scalarfunc.AddPoint(val[0],val[1],val[2],val[3])
                    
                gradientfunc = self.volumeProperty.GetGradientOpacity(0)
                gradientfunc.RemoveAllPoints()            
        
                gradientfuncsettings = volumepropertysettings.iter('gradientfuncsettings').next()
                for i in range(int(gradientfuncsettings.get('NumberOfPoints'))):
                    val = np.fromstring(gradientfuncsettings.get('pt'+str(i)),sep=",")
                    gradientfunc.AddPoint(val[0],val[1],val[2],val[3]) 
                    
                if tree.findall('labelsettings'):
                    labelsettings = tree.iter('labelsettings').next()
                    nlabels = int(labelsettings.get('NumberOfLabels'))
                    for i in range(nlabels):
                        val = np.fromstring(labelsettings.get('labelPos'+str(i)),sep=",")
                        self.labelText[i].SetText(labelsettings.get('labelText'+str(i)))
                        self.labelLine[i].SetEnabled(int(val[0]))
                        self.labelLineRep[i].SetPoint1WorldPosition([float(val[1]),float(val[2]),float(val[3])])
                        self.labelLineRep[i].SetPoint2WorldPosition([float(val[4]),float(val[5]),float(val[6])])
                 
                        self.labelControlItems.combobox_labels.setCurrentIndex(i)
                        self.changeLabelIndex(i)
                        self.displayLabel()
                        
                        
            self.lightingControlItems.button_shade.setChecked(False)
            self.setShade()			
            self._renWin.Render()

    def playCardiacCycle(self):
        if self.cb:
            if not self.isplay:
                self.isplay = True
                self.tag_observer1 = self._iren.AddObserver('TimerEvent', self.cb.execute)
                self._iren.CreateRepeatingTimer(20)
                self.cb.setplay(True)
            else:
                self.isplay = False
                self.cb.setplay(False)
                if not self.isrotate:
                    self._iren.RemoveObserver(self.tag_observer1)
    
    def rotateCamera(self):
        if self.cb:
            if not self.isrotate:
                self.isrotate = True
                self._iren.AddObserver('TimerEvent', self.cb.execute)
                self._iren.CreateRepeatingTimer(20)
                self.cb.setrotate(True)
            else:
                self.isrotate = False
                self.cb.setrotate(False)       
                if not self.isplay:
                    self._iren.RemoveObservers('TimerEvent')

    def editTransferFunction(self):
        if self.reader:
            self.transferFunctionEditor = TransferFunctionEditor(self.volumeProperty, self.reader, self._renWin)
            layout = pysideQGridLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            layout.addWidget(self.transferFunctionEditor.getTransferFunctionWidget())
             
            self.transferFunctionEditor.setLayout(layout)
            self.transferFunctionEditor.show()
                        
    def setStereoDepth(self, evt):
        self._ren.GetActiveCamera().SetEyeAngle(0.1*self.scale_stereodepth.value())
        self._renWin.Render()
        
    def setAzimuth(self, evt):
        val = self.scale_azimuth.value()
        self._ren.GetActiveCamera().Azimuth(self.varscaleazimuth-val)
        self.varscaleazimuth = val
        self._renWin.Render()
        
    def setElevation(self,evt):
        val = self.scale_elevation.value()
        self._ren.GetActiveCamera().Elevation(self.varscaleelevation-val)
        self.camerapos = self._ren.GetActiveCamera().OrthogonalizeViewUp() 
        self.varscaleelevation = val
        self._renWin.Render()

    def setRoll(self,evt):
        self._ren.GetActiveCamera().SetRoll(-1.0*self.scale_roll.value())
        self._renWin.Render()

    def zoomIn(self):
        self._ren.GetActiveCamera().Zoom(1.1)
        self._ren.ResetCameraClippingRange()
        self._renWin.Render()

    def zoomOut(self):
        self._ren.GetActiveCamera().Zoom(1.0/1.1)
        self._ren.ResetCameraClippingRange()
        self._renWin.Render()

    def resetCamera(self):
        self._ren.ResetCamera()
        self._ren.ResetCameraClippingRange()
        self._renWin.Render()
        
    def editOpacity(self):
        if self.reader:  
            self.opacityEditor = OpacityEditor(self.volumeProperty, self.reader, self._renWin)              
            self.opacityEditor.show()          

    def editGradientOpacity(self):
        if self.reader:                 
            self.opacityGradientEditor = GradientOpacityEditor(self.volumeProperty, self.reader, self._renWin)              
            self.opacityGradientEditor.show()   
    
    def editColor(self):
        if self.reader:     
            self.colorEditor = ColorEditor(self.volumeProperty, self.reader, self._renWin)
            self.colorEditor.show()
        
    def slider_imageNumber_valuechanged(self):
        if self.cb:
            self.label_imageNumber.setText(str(self.slider_imageNumber.value() + 1) + "/" + str(self.slider_imageNumber.maximum() + 1))    
            vol = self.volT[:, :, :, self.slider_imageNumber.value()]
            data_string = vol.flatten("A")
            self.reader.CopyImportVoidPointer(data_string, len(data_string)) 
            self.imageGaussianSmooth.Update()
            self._renWin.Render()    
            
    def smoothVolume(self):
        self.imageGaussianSmooth.SetStandardDeviations(0.1*self.slider_xsmooth.value(),0.1*self.slider_ysmooth.value(),0.1*self.slider_zsmooth.value())
        self.imageGaussianSmooth.Update()
        for label, slider in zip((self.label_xsmooth,self.label_ysmooth,self.label_zsmooth),(self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth)):
            label.setText(str(slider.value()*0.1))   
            
        self._renWin.Render()
        
    def setNoSmooth(self):
        for slider in (self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth):
            slider.setValue(0.0)
        
        self.smoothVolume()

    def setLowSmooth(self):
        for slider in (self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth):
            slider.setValue(1)
        
        self.smoothVolume()
        
    def setMidSmooth(self):
        for slider in (self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth):
            slider.setValue(10)
        
        self.smoothVolume()
        
    def setHighSmooth(self):
        for slider in (self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth):
            slider.setValue(50)
        
        self.smoothVolume()                

    def adjustLights(self):
        self.lightingControlItems.label_ambient.setText('Ambient: %.2f' % (0.01*self.lightingControlItems.slider_ambient.value()))
        self.lightingControlItems.label_diffuse.setText('Diffuse: %.2f' % (0.01*self.lightingControlItems.slider_diffuse.value()))
        self.lightingControlItems.label_specular.setText('Specular: %.2f' % (0.01*self.lightingControlItems.slider_specular.value()))
                
        self.volumeProperty.SetAmbient(0.01*self.lightingControlItems.slider_ambient.value())
        self.volumeProperty.SetDiffuse(0.01*self.lightingControlItems.slider_diffuse.value())
        self.volumeProperty.SetSpecular(0.01*self.lightingControlItems.slider_specular.value())     
        
#         self.volumeProperty.SetInterpolationTypeToLinear()

        self._renWin.Render()
        
    def setKeyLightIntensity(self):
        val = 0.2*self.lightingControlItems.slider_keylightintensity.value()
        
        self.lightingControlItems.label_keylightintensity.setText("Key Light Intensity: %.1f" % (val))
        
        self.lightkit.SetKeyLightIntensity(val)
        self._renWin.Render()
                
    def saveScreen(self):            
        options = QFileDialog.Options() | QFileDialog.DontUseNativeDialog
        filepath = QFileDialog.getSaveFileName(self,"Save Current Settings","","JPG Files (*.jpg)", "JPG Files (*.jpg)",options)
        if filepath:
            writer = vtk.vtkJPEGWriter()
            writer.SetFileName(str(filepath)+".jpg")
            writer.SetQuality(100)
            w2i = vtk.vtkWindowToImageFilter()
            w2i.SetInput(self._renWin)
            w2i.Update()
            writer.SetInputConnection(w2i.GetOutputPort())
            writer.Write()

    def changeView(self):
        str_button_view = self.sender().objectName()
#         self._ren.ResetCamera()        
        cam = self._ren.GetActiveCamera()
        focalpoint = cam.GetFocalPoint()
        dist = cam.GetDistance()
        
        
#         self.scale_roll.setValue(0)
        
#         print focalpoint, dist
#         cam = vtk.vtkCamera()
        if str_button_view == "button_view0":
            cam.SetPosition(focalpoint[0],focalpoint[1],focalpoint[2]+dist)
            cam.SetViewUp(0,1,0)
        if str_button_view == "button_view1":
            cam.SetPosition(focalpoint[0]+dist,focalpoint[1],focalpoint[2])
            cam.SetViewUp(0,1,0)
        if str_button_view == "button_view2":
            cam.SetPosition(focalpoint[0],focalpoint[1]+dist,focalpoint[2])
            cam.SetViewUp(0,0,-1)
        if str_button_view == "button_view3":
            cam.SetPosition(focalpoint[0],focalpoint[1],focalpoint[2]-dist)
            cam.SetViewUp(0,1,0)
        if str_button_view == "button_view4":
            cam.SetPosition(focalpoint[0]-dist,focalpoint[1],focalpoint[2])
            cam.SetViewUp(0,1,0)
        if str_button_view == "button_view5":
            cam.SetPosition(focalpoint[0],focalpoint[1]-dist,focalpoint[2])
            cam.SetViewUp(0,0,1)
        if str_button_view == "button_view6":
            self.scale_roll.setValue(0)
        if str_button_view == "button_view7":
            self.scale_roll.setValue(90)
        if str_button_view == "button_view8":
            self.scale_roll.setValue(180)
        if str_button_view == "button_view9":
            self.scale_roll.setValue(-90)
        
        self._renWin.Render()
         
    def displayLabel(self):
        i = self.labelControlItems.combobox_labels.currentIndex()        
        if self.labelControlItems.button_label.isChecked():
            self.labelActor[i].SetPosition(self.labelLineRep[i].GetPoint2WorldPosition())
            self.labelLine[i].EnabledOn()
            self._ren.AddActor(self.labelActor[i])
        else:
            self.labelLine[i].EnabledOff() 
            self._ren.RemoveActor(self.labelActor[i])
            
        self._renWin.Render()
        
    def changeLabelText(self):
        index = self.labelControlItems.combobox_labels.currentIndex()
        self.labelText[index].SetText(str(self.labelControlItems.text_label.text()))
        self._renWin.Render()
         
    def changeLabelSize(self):
        index = self.labelControlItems.combobox_labels.currentIndex()        
        self.labelActor[index].SetScale(0.1*self.labelControlItems.scale_labelsize.value())
        self._renWin.Render()
        
    def changeLabelIndex(self, value):
        self.labelControlItems.button_label.setChecked(self.labelLine[value].GetEnabled())
        self.labelControlItems.text_label.setText(self.labelText[value].GetText())
        
    def saveTransferFunction(self):
        
        root = ET.Element("TransferFunctions")          
        root.set("Type", "User")
        
        volumeProperty = ET.SubElement(root,"VolumeProperty")
        volumeProperty.set("InterpolationType", str(self.volumeProperty.GetInterpolationType()))
                
        component0 = ET.SubElement(volumeProperty, "Component")
        component0.set("Index", "0")
        component0.set("Shade", str(self.volumeProperty.GetShade(0)))
        component0.set("Ambient", str(self.volumeProperty.GetAmbient(0)))
        component0.set("Diffuse", str(self.volumeProperty.GetDiffuse(0)))
        component0.set("Specular", str(self.volumeProperty.GetSpecular(0)))
        component0.set("SpecularPower", str(self.volumeProperty.GetSpecularPower(0)))
        component0.set("DisableGradientOpacity", str(self.volumeProperty.GetDisableGradientOpacity(0)))
        component0.set("ComponentWeight", str(self.volumeProperty.GetComponentWeight(0)))
        component0.set("ScalarOpacityUnitDistance", str(self.volumeProperty.GetScalarOpacityUnitDistance(0)))
                                
        rgbtransferfuction = ET.SubElement(component0, "RGBTransferFunction")
        colorTransferFunction = ET.SubElement(rgbtransferfuction, "ColorTransferFunction")
        rgbfunction = self.volumeProperty.GetRGBTransferFunction(0)
        colorTransferFunction.set("Size",str(rgbfunction.GetSize()))
        
        nodeval = np.empty((6))
        for i in range(rgbfunction.GetSize()):
            rgbfunction.GetNodeValue(i, nodeval)
            point = ET.SubElement(colorTransferFunction, "Point")
            point.set("X",str(int(nodeval[0])))
            point.set("Value", "%f %f %f" % (nodeval[1], nodeval[2], nodeval[3]))
            point.set("MidPoint",str(int(nodeval[4])))
            point.set("Sharpness",str(int(nodeval[5])))

        scalarOpacity = ET.SubElement(component0, "ScalarOpacity")
        piecewiseFunction = ET.SubElement(scalarOpacity, "PiecewiseFunction")
        scalarfunction = self.volumeProperty.GetScalarOpacity(0)
        scalarOpacity.set("Size",str(scalarfunction.GetSize()))
        
        nodeval = np.empty((4))
        for i in range(scalarfunction.GetSize()):
            scalarfunction.GetNodeValue(i, nodeval)
            point = ET.SubElement(piecewiseFunction, "Point")
            point.set("X",str(int(nodeval[0])))
            point.set("Value", "%f" % nodeval[1])
            point.set("MidPoint",str(int(nodeval[2])))
            point.set("Sharpness",str(int(nodeval[3])))


        gradientOpacity = ET.SubElement(component0, "GradientOpacity")
        piecewiseFunction = ET.SubElement(gradientOpacity, "PiecewiseFunction")
        gradientfunction = self.volumeProperty.GetGradientOpacity(0)
        gradientOpacity.set("Size",str(gradientfunction.GetSize()))
        
        nodeval = np.empty((4))
        for i in range(gradientfunction.GetSize()):
            gradientfunction.GetNodeValue(i, nodeval)
            point = ET.SubElement(piecewiseFunction, "Point")
            point.set("X",str(int(nodeval[0])))
            point.set("Value", "%f" % nodeval[1])
            point.set("MidPoint",str(int(nodeval[2])))
            point.set("Sharpness",str(int(nodeval[3])))
                                
        
        tree = ET.ElementTree(root)        

        options = QFileDialog.Options() | QFileDialog.DontUseNativeDialog
        filepath = QFileDialog.getSaveFileName(self,"Save Transfer Function",tfuncdir+ os.sep + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"),"VVT Files (*.vvt)", "VVT Files (*.vvt)",options)
        if filepath:
            _, ext = os.path.splitext(str(filepath))
            if ext.upper() != '.VVT':
                filepath = '%s.vvt' % filepath
       
        tree.write(filepath)
        
        
        filedir, fname = os.path.split(str(filepath))
        
        self.transferFunctionControlItems.combobox_transfunction.clear()
    
        tfunc_files = glob.glob1(tfuncdir,"*.vvt")
        if tfunc_files:
            self.transferFunctionControlItems.combobox_transfunction.addItems(tfunc_files)   
            self.transferFunctionControlItems.combobox_transfunction.setCurrentIndex(self.transferFunctionControlItems.combobox_transfunction.findText(fname+".vvt"))        

    def eventFilter(self, obj, event):
        if(event.type() == QEvent.KeyPress):
            if event.key() == Qt.Key_Shift:
                self.labelMode = not self.labelMode
            if self.labelMode:
                self.modeText.SetInput("Labelling Mode")
            else:
                self.modeText.SetInput("Measuring Mode")
        return super(TDVizCustom, self).eventFilter(obj, event)

        self.distanceText.SetInput("distance = ")   
def main():        
    app = QApplication([])
    File = QFile("darkorange.stylesheet")
    File.open(QFile.ReadOnly)
    StyleSheet = QLatin1String(File.readAll())    
    app.setStyleSheet(StyleSheet)
    tdviz = TDVizCustom()
    tdviz.show() 

    if isprojector:
        tdviz.setGeometry(1920, 0, 1280, 1024)
    else:
#         tdviz.setGeometry(10, 10, 1280, 1024)
        tdviz.showFullScreen()    

    yscreenf = 1.0*tdviz._renWin.GetSize()[1]/1080.0
    cam = tdviz._ren.GetActiveCamera()
    cam.SetScreenBottomLeft(-262.5,148.5-148.5*2.0*yscreenf,-410)
    cam.SetScreenBottomRight(262.5,148.5-148.5*2.0*yscreenf,-410)
    cam.SetScreenTopRight(262.5,148.5,-410) 
    
#     myFilter = MyEventFilter()


    sys.exit(app.exec_())   
        
if __name__ == "__main__": 
     main()

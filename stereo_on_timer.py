from PyQt4.QtGui import QApplication, QFileDialog, QMessageBox, QInputDialog
from PyQt4.QtCore import QFile, QLatin1String
import sys
import os
import time
import datetime
import numpy as np

from TDViz_uiQt_cam import TDVizCustom

initddir = r'U:\Data\ProjectBrendan\Cases4Kumar'

MAX_TIME = 1800

class vtkTimerNextData():
    def __init__(self, master):
        self.start_time = time.time()
        self.master = master
    
    def execute(self, obj, event):
        time_elapsed = time.time() - self.start_time
        self.master.timertext.SetInput("time = %d" % (MAX_TIME - time_elapsed))
        obj.GetRenderWindow().Render()         
        
        if time_elapsed > MAX_TIME:
            self.master.loadEcho()
                    

class study_2dvs3d(TDVizCustom):
    def __init__(self, parent=None):
        TDVizCustom.__init__(self, parent)
        
        self.rootdir_ = None        
        self.button_loadEcho.setText("Next Study")
        self.button_stereo.setEnabled(False)
        self.user_time_dir = 'stereo_on_timer'
        
        
    def resetDistanceWidget(self):
        self.distanceWidget.EnabledOn()
        self.distanceWidget.GetDistanceRepresentation().SetPoint1WorldPosition(np.array([0,0,100]))
        self.distanceWidget.GetDistanceRepresentation().SetPoint2WorldPosition(np.array([0,0,50]))
        self.distanceWidget.EnabledOff()
        
        self.loadSettings()
        
        timerNext = vtkTimerNextData(self)
        self._iren.AddObserver('TimerEvent', timerNext.execute)
        timerNextId = self._iren.CreateRepeatingTimer(20)        
    
                
    def loadDir(self):
        
        if not os.path.isdir(self.user_time_dir):
            os.mkdir(self.user_time_dir)        
        
        user_name, ok = QInputDialog.getText(self, 'User information', 'Enter your name:')
        
        if ok and str(user_name).strip():
            self.user_filename = str(user_name).replace(" ", "_") + ".txt" 
                  
        
        options = QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly | QFileDialog.DontUseNativeDialog
        dirname = str(QFileDialog.getExistingDirectory(self,"Select DICOM Directory", initddir, options))

        if dirname:
            self.rootdir_, self.dirlist_, _ = os.walk(dirname).next()
            
            if not self.dirlist_:
                dirname = os.path.dirname(dirname)
                self.rootdir_, self.dirlist_, _  = os.walk(dirname).next()            
            
            self.dir_id = 0
            
            super(study_2dvs3d, self).loadDir(os.path.join(self.rootdir_,self.dirlist_[0]))
            self.resetDistanceWidget()


            
    
        
    def loadEcho(self):
        if self.rootdir_:
            self.save_user_time()
            self.dir_id += 1            
            if self.dir_id < len(self.dirlist_):                
                super(study_2dvs3d, self).loadDir(os.path.join(self.rootdir_,self.dirlist_[self.dir_id]))
                self.resetDistanceWidget()
                                
            else:
                msgBox = QMessageBox()
                msgBox.setText("This is the last study in the selected directory!")
                msgBox.exec_() 
                self.close()
    
    def save_user_time(self):
        if self.dir_id < len(self.dirlist_):
            distw_p1, distw_p2, label1_pt, label2_pt  = [np.empty((3)) for i in range(4)]
            
            self.distanceRep.GetPoint1WorldPosition(distw_p1)
            self.distanceRep.GetPoint2WorldPosition(distw_p2)
            
            self.labelLineRep[0].GetPoint1WorldPosition(label1_pt)
            self.labelLineRep[1].GetPoint1WorldPosition(label2_pt)
                        
            text = "test_time=%s, dir=%s, user_time=%.4f, dwidget_p1=(%.2f, %.2f, %.2f), dwidget_p2=(%.2f, %.2f, %.2f), label1_pt=(%.2f, %.2f, %.2f), label2_pt=(%.2f, %.2f, %.2f)\n" % (
                                                                                              datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"), 
                                                                                              str(self.dirname), time.time() - self.time_rendered, distw_p1[0], distw_p1[1], distw_p1[2],
                                                                                              distw_p2[0], distw_p2[1], distw_p2[2],
                                                                                              label1_pt[0], label1_pt[1], label1_pt[2],
                                                                                              label2_pt[0], label2_pt[1], label2_pt[2])
                       
            f_usertime = file(os.path.join(self.user_time_dir,self.user_filename), 'a')
            f_usertime.write(text)
            f_usertime.close()            
            
            
#             print self.dir_id, time.time() - self.time_rendered


def main():        
    app = QApplication([])

    File = QFile("darkorange.stylesheet")
    File.open(QFile.ReadOnly)
    StyleSheet = QLatin1String(File.readAll())    
 
    app.setStyleSheet(StyleSheet)

    tdviz = study_2dvs3d()
    tdviz.show()

    tdviz.showFullScreen()    

    yscreenf = 1.0*tdviz._renWin.GetSize()[1]/1080.0

    cam = tdviz._ren.GetActiveCamera()
    cam.SetScreenBottomLeft(-262.5,148.5-148.5*2.0*yscreenf,-410)
    cam.SetScreenBottomRight(262.5,148.5-148.5*2.0*yscreenf,-410)
    cam.SetScreenTopRight(262.5,148.5,-410) 

    sys.exit(app.exec_())   
        
if __name__ == "__main__": 
     main()
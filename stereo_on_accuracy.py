from PyQt4.QtGui import QApplication, QFileDialog, QMessageBox, QInputDialog
from PyQt4.QtCore import QFile, QLatin1String
import sys
import os
import time
import datetime
import numpy as np
import vtk
from TDViz_uiQt_cam import TDVizCustom


initddir = r'U:\Data\ProjectBrendan\Cases4Kumar'

class study_2dvs3d(TDVizCustom):
    def __init__(self, parent=None):
        TDVizCustom.__init__(self, parent)
        
        self.user_time_dir = 'stereo_on_fixed_accuracy'
        
        self.rootdir_ = None        
        self.button_loadEcho.setText("Next Study")
        self.button_stereo.setEnabled(False)
               
        self.last_distw_p1, self.last_distw_p2, self.distw_p1, self.distw_p2 = [np.empty((3)) for i in range(4)]
        self.label1_pt, self.label2_pt  = [np.empty((3)) for i in range(2)]
        
        
    def resetDistanceWidget(self):
        self.distanceWidget.EnabledOn()
        self.distanceWidget.GetDistanceRepresentation().SetPoint1WorldPosition(np.array([0,0,100]))
        self.distanceWidget.GetDistanceRepresentation().SetPoint2WorldPosition(np.array([0,0,50]))
        self.distanceWidget.EnabledOff()
        
        if str(self.combobox_loadsettings.itemText(0)):            
            self.loadSettings()
            self._ren.ResetCameraClippingRange()
                
    def loadDir(self):
        
        if not os.path.isdir(self.user_time_dir):
            os.mkdir(self.user_time_dir)        
        
        user_name, ok = QInputDialog.getText(self, 'User information', 'Enter your name:')
        
        if ok and str(user_name).strip():
            self.user_filename = str(user_name).replace(" ", "_") + ".txt" 
                  
        
        options = QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly | QFileDialog.DontUseNativeDialog
        dirname = str(QFileDialog.getExistingDirectory(self,"Select DICOM Directory", initddir, options))

        if dirname:
            self.rootdir_, self.dirlist_, _ = os.walk(dirname).next()
            
            if not self.dirlist_:
                dirname = os.path.dirname(dirname)
                self.rootdir_, self.dirlist_, _  = os.walk(dirname).next()            
            
            self.dir_id = 0
            
            super(study_2dvs3d, self).loadDir(os.path.join(self.rootdir_,self.dirlist_[0]))
            self.resetDistanceWidget()
        
    def loadEcho(self):
        if self.rootdir_:
            self.save_user_time()
            self.dir_id += 1            
            if self.dir_id < len(self.dirlist_):                
                super(study_2dvs3d, self).loadDir(os.path.join(self.rootdir_,self.dirlist_[self.dir_id]))
                self.resetDistanceWidget()                
            else:
                msgBox = QMessageBox()
                msgBox.setText("This is the last study in the selected directory!")
                msgBox.exec_() 
    
    def save_user_time(self):
        if self.dir_id < len(self.dirlist_):
            distw_p1, distw_p2, label1_pt, label2_pt  = [np.empty((3)) for i in range(4)]
            
            self.distanceRep.GetPoint1WorldPosition(distw_p1)
            self.distanceRep.GetPoint2WorldPosition(distw_p2)
            
            self.labelLineRep[0].GetPoint1WorldPosition(label1_pt)
            self.labelLineRep[1].GetPoint1WorldPosition(label2_pt)
                        
            text = "test_time=%s, dir=%s, user_time=%.4f, dwidget_p1=(%.2f, %.2f, %.2f), dwidget_p2=(%.2f, %.2f, %.2f), label1_pt=(%.2f, %.2f, %.2f), label2_pt=(%.2f, %.2f, %.2f)\n" % (
                                                                                              datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"), 
                                                                                              str(self.dirname), time.time() - self.time_rendered, distw_p1[0], distw_p1[1], distw_p1[2],
                                                                                              distw_p2[0], distw_p2[1], distw_p2[2],
                                                                                              label1_pt[0], label1_pt[1], label1_pt[2],
                                                                                              label2_pt[0], label2_pt[1], label2_pt[2])
                       
            f_usertime = file(os.path.join(self.user_time_dir,self.user_filename), 'a')
            f_usertime.write(text)
            f_usertime.close()            
            
            
#             print self.dir_id, time.time() - self.time_rendered

    #sets threshold and saves dw starting positions
    def dwStartInteraction(self, obj, event):
        self.distanceRep.GetPoint1WorldPosition(self.last_distw_p1)
        self.distanceRep.GetPoint2WorldPosition(self.last_distw_p2)
        self.labelLineRep[0].GetPoint1WorldPosition(self.label1_pt)
        self.labelLineRep[1].GetPoint1WorldPosition(self.label2_pt)  
        
        self.label_sqr_dist_thresh = (1.0 * self.distanceScale)**2
#         super(study_2dvs3d,self).dwStartInteraction(obj, event)

    
    def dwEndInteraction(self, obj, event):            
        self.distanceRep.GetPoint1Representation().GetSelectedProperty().SetColor(0.0,1.0,0)
#         super(study_2dvs3d,self).dwEndInteraction(obj, event)
        
    def dwUpdateMeasurement(self, obj, event):
        self.distanceRep.GetPoint1WorldPosition(self.distw_p1)
        self.distanceRep.GetPoint2WorldPosition(self.distw_p2)
        
        #find the point that moved
        if vtk.vtkMath.Distance2BetweenPoints(self.distw_p1, self.last_distw_p1) > vtk.vtkMath.Distance2BetweenPoints(self.distw_p2, self.last_distw_p2):
            pt = self.distw_p1
        else:
            pt = self.distw_p2
        
        #find the distance to the nearest label
        label_dist = self.findLabelPointDistance(pt)         
        if label_dist < self.label_sqr_dist_thresh:
            self.distanceRep.GetPoint1Representation().GetSelectedProperty().SetColor(1.0,0.0,0)
        else:
            self.distanceRep.GetPoint1Representation().GetSelectedProperty().SetColor(0.0,1.0,0)
        
        
#         print 'thresh=' + str(self.label_sqr_dist_thresh) + '   dist1=' + str(self.findLabelPointDistance(self.distw_p1)) + '   dist2=' + str(self.findLabelPointDistance(self.distw_p2))
        
        if self.findLabelPointDistance(self.distw_p1) < self.label_sqr_dist_thresh and self.findLabelPointDistance(self.distw_p2) < self.label_sqr_dist_thresh:
            self.distanceRep.GetPoint1Representation().GetProperty().SetColor(1.,0.,0.)
            self.distanceRep.GetLabelProperty().SetColor(1.,0.,0.)
            self.distanceRep.GetLineProperty().SetColor(1.,1.,0.)
            self.distanceRep.GetGlyphActor().GetProperty().SetColor(1.,0.,0.)
        else:
            self.distanceRep.GetPoint1Representation().GetProperty().SetColor(1.,1.,1.)
            self.distanceRep.GetLabelProperty().SetColor(1.,1.,1.)
            self.distanceRep.GetLineProperty().SetColor(0.,0.,1.)
            self.distanceRep.GetGlyphActor().GetProperty().SetColor(1.,1.,1.)                 
            
#         super(study_2dvs3d,self).dwUpdateMeasurement(obj, event)
        self.distanceText.SetInput("distance = %-#6.3g mm" % (self.distanceRep.GetDistance() / self.distanceScale))
        
    def updateDistanceText(self):
        self.distanceText.SetInput("distance = %-#6.3g mm" % (self.distanceRep.GetDistance() / self.distanceScale))
        
    def findLabelPointDistance(self, pt):        
        dist1, dist2 = vtk.vtkMath.Distance2BetweenPoints(self.label1_pt, pt), vtk.vtkMath.Distance2BetweenPoints(self.label2_pt, pt)
        
        return dist1 if dist1 < dist2 else dist2      


def main():        
    app = QApplication([])

    File = QFile("darkorange.stylesheet")
    File.open(QFile.ReadOnly)
    StyleSheet = QLatin1String(File.readAll())    
 
    app.setStyleSheet(StyleSheet)

    tdviz = study_2dvs3d()
    tdviz.show()

    tdviz.showFullScreen()
    app.installEventFilter(tdviz)      

    yscreenf = 1.0*tdviz._renWin.GetSize()[1]/1080.0

    cam = tdviz._ren.GetActiveCamera()
    cam.SetScreenBottomLeft(-262.5,148.5-148.5*2.0*yscreenf,-410)
    cam.SetScreenBottomRight(262.5,148.5-148.5*2.0*yscreenf,-410)
    cam.SetScreenTopRight(262.5,148.5,-410) 

    sys.exit(app.exec_())   
        
if __name__ == "__main__": 
     main()